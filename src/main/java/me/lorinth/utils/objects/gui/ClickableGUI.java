package me.lorinth.utils.objects.gui;

import me.lorinth.utils.objects.Pair;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;

public class ClickableGUI implements Listener {

    private Plugin plugin;
    private Inventory inventory;
    private Player viewer;
    private int slots;
    private String title;
    private ArrayList<InventoryIconAction> iconActions = new ArrayList<>();

    public ClickableGUI(Plugin plugin, int rows, String title){
        this.plugin = plugin;
        this.slots = rows * 9;
        this.title = title;
        inventory = Bukkit.createInventory(null, slots, title);
    }

    public void addIcon(ItemStack icon, Runnable action){
        addIcon(icon, inventory.firstEmpty(), action);
    }

    public void addIcon(ItemStack icon, Pair<ClickType, Runnable>... actions){
        addIcon(icon, inventory.firstEmpty(), actions);
    }

    public void addIcon(ItemStack icon, int slot, Runnable action){
        addIcon(icon, slot, new Pair<>(ClickType.LEFT, action));
    }

    public void addIcon(ItemStack icon, int slot, Pair<ClickType, Runnable>... actions){
        if(!(slot < slots)){
            return;
        }

        if(slot == -1){
            inventory.addItem(icon);
        }
        else{
            inventory.setItem(slot, icon);
        }

        iconActions.add(new InventoryIconAction(icon, actions));
    }

    public void open(Player player){
        viewer = player;
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    private boolean isViewer(Player player){
        return viewer.getUniqueId().toString().equalsIgnoreCase(player.getUniqueId().toString());
    }

    public void close(Player p) {
        if (p.getOpenInventory().getTitle().equals(title)) {
            p.closeInventory();
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        Inventory clickedInventory = event.getClickedInventory();
        if(clickedInventory == null){
            return;
        }

        InventoryView view = event.getView();
        Player clicker = (Player) event.getWhoClicked();

        ItemStack clicked = event.getCurrentItem();
        if(clicked == null){
            return;
        }

        if(!view.getTitle().equalsIgnoreCase(title) || !isViewer(clicker)){
            return;
        }

        event.setCancelled(true);
        for(InventoryIconAction iconAction : iconActions){
            if(iconAction.itemMatches(clicked)){
                iconAction.execute(event.getClick());
                break;
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event){
        Player player = (Player) event.getPlayer();
        if(!isViewer(player)){
            return;
        }

        HandlerList.unregisterAll(this);
    }

    private class InventoryIconAction{

        private ItemStack item;
        private Pair<ClickType, Runnable>[] actions;

        public InventoryIconAction(ItemStack item, Pair<ClickType, Runnable> ...actions){
            this.item = item;
            this.actions = actions;

        }

        public boolean itemMatches(ItemStack other){
            return item.isSimilar(other);
        }

        public void execute(ClickType clickType){
            for(Pair<ClickType, Runnable> action : actions){
                if(action.getKey() == clickType){
                    action.getValue().run();
                }
            }
        }

    }

}
