package me.lorinth.utils.objects;

import org.bukkit.Material;

public class MaterialDurability {

    private Material material;
    private Short durability;

    public MaterialDurability(Material material){
        this.material = material;
    }

    public MaterialDurability(Material material, Short durability){
        this.material = material;
        this.durability = durability;
    }

    public Material getMaterial(){
        return material;
    }

    public Short getDurability(){
        return durability;
    }

}
