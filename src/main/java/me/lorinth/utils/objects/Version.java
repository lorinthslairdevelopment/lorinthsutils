package me.lorinth.utils.objects;

import me.lorinth.utils.TryParse;

public class Version {

    private Integer major = 0;
    private Integer minor = 0;
    private Integer build = 0;

    public Version(String value){
        String[] splitgarbageValues = value.split(";");
        String[] splitSnapshotValues = splitgarbageValues[0].split("-");
        String[] values = splitSnapshotValues[0].split("\\.");
        if(values.length > 0){
            if(TryParse.parseInt(values[0])){
                major = Integer.parseInt(values[0]);
            }
        }
        if(values.length > 1){
            if(TryParse.parseInt(values[1])){
                minor = Integer.parseInt(values[1]);
            }
        }
        if(values.length > 2){
            if(TryParse.parseInt(values[2])){
                build = Integer.parseInt(values[2]);
            }
        }
    }

    public Version(Integer major, Integer minor, Integer build){
        this.major = major;
        this.minor = minor;
        this.build = build;
    }

    public Integer getMajorVersion(){
        return major;
    }

    public Integer getMinorVersion(){
        return minor;
    }

    public Integer getBuildVersion(){
        return build;
    }

}
