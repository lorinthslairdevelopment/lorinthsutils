package me.lorinth.utils;

import com.google.gson.Gson;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONObject;

public class JsonHelper {

    public static JSONObject parseLocationToJSON(Location location){
        JSONObject json = new JSONObject();
        json.put("World", location.getWorld().getName());
        json.put("X", location.getX());
        json.put("Y", location.getY());
        json.put("Z", location.getZ());
        json.put("Yaw", location.getYaw());
        json.put("Pitch", location.getPitch());
        return json;
    }

    public static Location parseLocationFromJSON(JSONObject json){
        String worldName = (String) json.get("World");
        double x = (double) json.get("X");
        double y = (double) json.get("Y");
        double z = (double) json.get("Z");
        float yaw = (float) (double) json.get("Yaw");
        float pitch = (float) (double) json.get("Pitch");

        World world = Bukkit.getWorld(worldName);
        if(world != null)
            return new Location(world, x, y, z, yaw, pitch);

        return null;
    }

    public static String parseItemStackToJSON(ItemStack itemStack){
        Gson gson = new Gson();
        return gson.toJson(itemStack);
    }

    public static ItemStack parseItemStackFromJSON(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, ItemStack.class);
    }

}
