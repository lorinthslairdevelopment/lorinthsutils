package me.lorinth.utils;

/**
 * Math utility methods
 */
public class MathHelper {

    /**
     * Double value representing margin of accepted error
     */
    private final static double EPSILON = 0.000001;

    /**
     * Compare two doubles, using default epsilon
     * @param firstNumber Double value representing first number
     * @param secondNumber Double value representing second number
     * @return Whether or not two numbers are equal
     */
    public static boolean equals(double firstNumber, double secondNumber) {
        if (firstNumber == secondNumber) return true;
        // If the difference is less than epsilon, treat as equal.
        return Math.abs(firstNumber - secondNumber) <= EPSILON;
    }

    public static boolean isZero(Object value){
        if(value instanceof Double){
            return equals(0.0d, (Double) value);
        }

        return false;
    }

}
