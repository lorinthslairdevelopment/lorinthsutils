package me.lorinth.utils;

import me.lorinth.utils.javascript.JavascriptEngine;

import java.util.HashMap;
import java.util.Map;

public class BooleanCalculator {

    private static FormulaMethod formulaMethod;

    private enum BooleanOperator{
        AND,
        OR,
        NOT_EQUALS,
        EQUALS,
        LESS_THAN_OR_EQUAL,
        LESS_THAN,
        GREATER_THAN_OR_EQUAL,
        GREATER_THAN
    }

    private static HashMap<String, BooleanOperator> symbols = new HashMap<String, BooleanOperator>(){{
        //put("!==", BooleanOperator.NOT_EQUALS);
        //put("&&", BooleanOperator.AND);
        //put("||", BooleanOperator.OR);
        //put("==", BooleanOperator.EQUALS);
        put("<=", BooleanOperator.LESS_THAN_OR_EQUAL);
        put(">=", BooleanOperator.GREATER_THAN_OR_EQUAL);
        put("<", BooleanOperator.LESS_THAN);
        put(">", BooleanOperator.GREATER_THAN);
    }};

    private static char[] logicCharacters = "!=&|<>".toCharArray();

    public static boolean evaluate(FormulaMethod formulaMethod, String expression){
        if(formulaMethod == FormulaMethod.Javascript){
            return JavascriptEngine.parseBooleanExpression(expression);
        }

        BooleanCalculator.formulaMethod = formulaMethod;

        if(!hasLogicOperator(expression))
            return Calculator.eval(BooleanCalculator.formulaMethod, expression) > 0;

        //Check for sub statements
        if(expression.contains("(")){
            int subExpressionIndex = expression.indexOf('(');

            while(subExpressionIndex > -1){
                String subExpression = getSubExpression(subExpressionIndex+1, expression);
                if(hasLogicOperator(subExpression)){
                    boolean isTrue = BooleanCalculator.evaluate(formulaMethod, subExpression);
                    expression = expression.substring(0, subExpressionIndex) +
                            (isTrue ? "1" : "0")
                            + expression.substring(subExpressionIndex + subExpression.length()+2);
                }

                subExpressionIndex = expression.indexOf('(', subExpressionIndex + subExpression.length());
            }
        }

        char cha;
        //Handle logic in order
        for(int i=0; i<expression.length(); i++){
            cha = expression.charAt(i);
            if(isLogicCharacter(cha)){
                try{
                    Map.Entry<String, BooleanOperator> logicResult = getLogicOperator(i, expression);
                    String key = logicResult.getKey();
                    String left = expression.substring(0, i).trim();
                    String right = expression.substring(i+key.length()).trim();
                    switch(logicResult.getValue()){
                        case OR:
                            return handleOr(left, right);
                        case AND:
                            return handleAnd(left, right);
                        case EQUALS:
                            return handleEquals(left, right);
                        case LESS_THAN:
                            return handleLessThan(left, right);
                        case LESS_THAN_OR_EQUAL:
                            return handleLessThanOrEqual(left, right);
                        case GREATER_THAN:
                            return handleGreaterThan(left, right);
                        case GREATER_THAN_OR_EQUAL:
                            return handleGreaterThanOrEqual(left, right);
                        case NOT_EQUALS:
                            return handleNotEqual(left, right);
                    }
                }
                catch(Exception ex){
                    System.out.println("Error parsing truthy formula");
                    System.out.print(ex.toString());
                }
            }
        }

        return false;
    }

    private static boolean hasLogicOperator(String expression){
        boolean operatorFound = false;
        for(String key : symbols.keySet()){
            operatorFound = expression.contains(key);

            if(operatorFound)
                break;
        }

        return operatorFound;
    }

    private static boolean handleOr(String left, String right){
        boolean isTrue = Calculator.eval(BooleanCalculator.formulaMethod, left) > 0;
        boolean evaluteRest = BooleanCalculator.evaluate(BooleanCalculator.formulaMethod, right);
        return isTrue || evaluteRest;
    }

    private static boolean handleAnd(String left, String right){
        boolean isTrue = Calculator.eval(BooleanCalculator.formulaMethod, left) > 0;
        boolean evaluteRest = BooleanCalculator.evaluate(BooleanCalculator.formulaMethod, right);
        return isTrue && evaluteRest;
    }

    private static boolean handleEquals(String left, String right){
        Double a = Calculator.eval(BooleanCalculator.formulaMethod, left);
        Double b = Calculator.eval(BooleanCalculator.formulaMethod, right);
        return a.equals(b);
    }

    private static boolean handleLessThan(String left, String right){
        Double a = Calculator.eval(BooleanCalculator.formulaMethod, left);
        Double b = Calculator.eval(BooleanCalculator.formulaMethod, right);
        return a < b;
    }

    private static boolean handleGreaterThan(String left, String right){
        Double a = Calculator.eval(BooleanCalculator.formulaMethod, left);
        Double b = Calculator.eval(BooleanCalculator.formulaMethod, right);
        return a > b;
    }

    private static boolean handleLessThanOrEqual(String left, String right){
        Double a = Calculator.eval(BooleanCalculator.formulaMethod, left);
        Double b = Calculator.eval(BooleanCalculator.formulaMethod, right);
        return a < b || a.equals(b);
    }

    private static boolean handleGreaterThanOrEqual(String left, String right){
        Double a = Calculator.eval(BooleanCalculator.formulaMethod, left);
        Double b = Calculator.eval(BooleanCalculator.formulaMethod, right);
        return a > b || a.equals(b);
    }

    private static boolean handleNotEqual(String left, String right){
        Double a = Calculator.eval(BooleanCalculator.formulaMethod, left);
        Double b = Calculator.eval(BooleanCalculator.formulaMethod, right);
        return !a.equals(b);
    }

    private static String getSubExpression(int start, String expression){
        int openParens = 1;
        for(int i=start; i<expression.length(); i++){
            char cha = expression.charAt(i);

            if(cha == '(') openParens++;
            else if(cha == ')') openParens--;

            if(openParens == 0) {
                return expression.substring(start, i);
            }
        }
        throw new RuntimeException("The truthy function has no closing brace in, " + expression);
    }

    private static boolean isLogicCharacter(char letter){
        for(char logicChar : logicCharacters){
            if(logicChar == letter)
                return true;
        }
        return false;
    }

    private static Map.Entry<String, BooleanOperator> getLogicOperator(int i, String expression){
        for(Map.Entry<String, BooleanOperator> entry : symbols.entrySet()){
            String key = entry.getKey();
            String subString = expression.substring(i, i + key.length());
            if(key.equalsIgnoreCase(subString))
                return entry;
        }

        throw new RuntimeException("Logical operator not recognized, " + expression);
    }

}
