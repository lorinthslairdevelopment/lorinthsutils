package me.lorinth.utils;

/**
 * Parsing utility methods
 */
public class TryParse {

    /**
     * Check if value is parsable to int
     * @param value String value to parse
     * @return Whether or not value is parsable to int
     */
    public static boolean parseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if value is parsable to double
     * @param value String value to parse
     * @return Whether or not value is parsable to double
     */
    public static boolean parseDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if value is parsable to float
     * @param value String value to parse
     * @return Whether or not value is parsable to float
     */
    public static boolean parseFloat(String value) {
        try {
            Float.parseFloat(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if value is parsable to boolean
     * @param value String value to parse
     * @return Whether or not value is parsable to boolean
     */
    public static boolean parseBoolean(String value){
        try {
            Boolean.parseBoolean(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if value is parsable to Enum
     * @param clazz Enum class
     * @param value String value to parse
     * @return Whether or not value is parsable to boolean
     */
    public static boolean parseEnum(Class clazz, String value){
        try{
            return Enum.valueOf(clazz, value) != null;
        } catch(Exception e){
            return false;
        }
    }

}
