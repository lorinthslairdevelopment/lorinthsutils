package me.lorinth.utils;

import org.bukkit.Location;
import org.bukkit.util.Vector;

/**
 * Class with Vector utility functions.
 */
public class VectorHelper {

    /**
     * Get vector between 2 locations
     * @param from Vector origin point
     * @param to Vector end point
     * @return Vector between 2 locations
     */
    public static Vector getRawVector(Location from, Location to){
        return to.toVector().subtract(from.toVector());
    }

    /**
     * Get normalized (divided by its length) vector between 2 locations
     * @param from Vector origin point
     * @param to Vector end point
     * @return Normalized vector between 2 locations
     */
    public static Vector getNormalizedVector(Location from, Location to){
        return getRawVector(from, to).normalize();
    }

    /**
     * Get normalized vector between 2 locations multiplied by force
     * @param from Vector origin point
     * @param to Vector end point
     * @param force Desired vector multiplier
     * @return Normalized vector between 2 locations multiplied by force
     */
    public static Vector getDirectionVector(Location from, Location to, double force){
        return getNormalizedVector(from, to).multiply(force);
    }

}
