package me.lorinth.utils.javascript;

import me.lorinth.utils.OutputHandler;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.*;

public class JavascriptEngine {

    private static ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
    private static ScriptEngine scriptEngine;
    private static boolean isLoaded = true;

    public JavascriptEngine(Class c, String directory, OutputHandler outputHandler){
        ClassLoader cl = c.getClassLoader();
        Thread.currentThread().setContextClassLoader(cl);
        if(scriptEngine == null){
            scriptEngine = scriptEngineManager.getEngineByName("javascript");
        }
        if(scriptEngine == null) {
            scriptEngine = scriptEngineManager.getEngineByName("nashorn");
        }
        if(scriptEngine == null) {
            outputHandler.printError("Can't find script engine javascript or nashorn. Disabling Javascript functionality");
            return;
        }
        isLoaded = true;
        File dir = new File(directory);
        if(dir.isDirectory() && dir.exists()){
            for(File file : new File(directory).listFiles()){
                try{
                    InputStream is = new FileInputStream(file);
                    BufferedReader buf = new BufferedReader(new InputStreamReader(is));
                    String line = buf.readLine();
                    StringBuilder sb = new StringBuilder();
                    while(line != null){
                        sb.append(line).append("\n");
                        line = buf.readLine();
                    }
                    buf.close();
                    is.close();

                    scriptEngine.eval(sb.toString());
                    outputHandler.printInfo("Loaded js file, " + file.getName());
                }
                catch(Exception exception){
                    exception.printStackTrace();
                }
            }
        }
    }

    public static boolean isLoaded(){
        return isLoaded;
    }

    public static void put(String key, Object value){
        try{
            scriptEngine.put(key, value);
        }
        catch(Exception exception){
            exception.printStackTrace();
        }
    }

    public static Object eval(String expression){
        try{
            return scriptEngine.eval(expression);
        }catch(Exception exception){
            exception.printStackTrace();
        }
        return null;
    }

    public static double parseValue(String expression){
        try{
            Object result = scriptEngine.eval(expression);
            return (double) result;
        } catch(Exception exception){
            exception.printStackTrace();
        }
        return 0.0;
    }

    public static boolean parseBooleanExpression(String expression){
        try{
            Object result = scriptEngine.eval(expression);
            return (boolean) result;
        } catch(Exception exception){
            exception.printStackTrace();
        }
        return false;
    }

}
