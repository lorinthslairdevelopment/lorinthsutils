package me.lorinth.utils.gson.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.IOException;

public class LocationAdapter extends TypeAdapter<Location> {
    @Override
    public void write(JsonWriter jsonWriter, Location location) throws IOException {
        jsonWriter.beginObject();
        jsonWriter.name("x");
        jsonWriter.value(location.getX());
        jsonWriter.name("y");
        jsonWriter.value(location.getY());
        jsonWriter.name("z");
        jsonWriter.value(location.getZ());
        jsonWriter.name("yaw");
        jsonWriter.value(location.getYaw());
        jsonWriter.name("pitch");
        jsonWriter.value(location.getPitch());
        if (location.getWorld() != null){
            jsonWriter.name("worldName");
            jsonWriter.value(location.getWorld().getName());
        }
        else {
            throw new IOException("Not a valid world");
        }
        jsonWriter.endObject();
    }

    @Override
    public Location read(JsonReader jsonReader) throws IOException {
        double x = 0;
        double y = 0;
        double z = 0;
        float yaw = 0;
        float pitch = 0;
        String worldName = "";

        String fieldname = "";
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            JsonToken token = jsonReader.peek();

            if (token.equals(JsonToken.NAME)) {
                //get the current token
                fieldname = jsonReader.nextName();
            }

            if ("x".equals(fieldname)) {
                jsonReader.peek();
                x = jsonReader.nextDouble();
            }

            if("y".equals(fieldname)) {
                jsonReader.peek();
                y = jsonReader.nextDouble();
            }

            if("z".equals(fieldname)) {
                jsonReader.peek();
                z = jsonReader.nextDouble();
            }

            if("yaw".equals(fieldname)) {
                jsonReader.peek();
                yaw = (float) jsonReader.nextDouble();
            }

            if("pitch".equals(fieldname)) {
                jsonReader.peek();
                pitch = (float) jsonReader.nextDouble();
            }

            if ("worldName".equalsIgnoreCase(fieldname)){
                jsonReader.peek();
                worldName = jsonReader.nextString();
            }
        }

        jsonReader.endObject();
        World world = Bukkit.getWorld(worldName);
        if (world == null){
            throw new IOException("Not a valid world");
        }
        return new Location(world, x, y, z, yaw, pitch);
    }
}
