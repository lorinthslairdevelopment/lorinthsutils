package me.lorinth.utils.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.lorinth.utils.gson.adapters.LocationAdapter;
import org.bukkit.Location;

public class MinecraftGsonFactory {

    public static Gson buildMinecraftGsonAndCreate(){
        return new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(Location.class, new LocationAdapter())
                .create();
    }

    public static GsonBuilder buildMinecraftGson(){
        return new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(Location.class, new LocationAdapter());
    }
}
