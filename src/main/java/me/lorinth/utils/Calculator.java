package me.lorinth.utils;

import me.lorinth.utils.javascript.JavascriptEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Used to do math evaluations of string formulas
 */
public class Calculator {
    private static Random random = new Random();

    //Allow for seemless change between basic and JS using basic syntax
    private static String preParseJavaScript(String formula){
        return formula.replace("rand(", "Math.floor(Math.random() * ")
                .replace("min(", "Math.min(")
                .replace("max(", "Math.max(")
                .replace("abs(", "Math.max(");
    }

    private static String preParseJava(String formula){
        return formula.replace("math.floor(math.random() * ", "rand(")
                .replace("math.min(", "min(")
                .replace("math.min(", "min(")
                .replace("math.max(", "max(")
                .replace("math.max(", "max(");
    }

    public String toString(){
        return "static [me.lorinth.utils.Calculator]";
    }

    /**
     * Evaluates a string formula supports some functions
     *      sin, cos, tan, random(Rand, rand)
     * @param formula - formula to evaluate
     * @return - result
     */
    public static double eval(FormulaMethod formulaMethod, String formula) {
        if(formulaMethod == FormulaMethod.Javascript){
            return JavascriptEngine.parseValue(preParseJavaScript(formula));
        }

        formula = formula.toLowerCase();
        final String str = preParseJava(formula);

        return new Object() {
            int ch, pos = -1;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()){
                    throw new RuntimeException("Unexpected: " + (char)ch + " at index " + pos);
                }
                return x;
            }

            String getFullParameterString(){
                int openParens = 1;
                for(int i=this.pos; i<str.length(); i++){
                    char cha = str.charAt(i);

                    if(cha == '(') openParens++;
                    else if(cha == ')') openParens--;

                    if(openParens == 0) {
                        String params = str.substring(this.pos, i);
                        this.pos = i;
                        this.ch = ')';
                        return params;
                    }
                }
                throw new RuntimeException("The function has no closing brace in, " + str);
            }

            ArrayList<String> getParameters(String fullParameterString){
                ArrayList<String> parameters = new ArrayList<>();

                int openParens = 0;
                int start = 0;
                for(int i=0; i<fullParameterString.length(); i++){
                    char cha = fullParameterString.charAt(i);

                    if(cha == ',' && openParens == 0){
                        parameters.add(fullParameterString.substring(start, i));
                        start = i+1;
                    }
                    else if(cha == '('){
                        openParens++;
                    }
                    else if(cha == ')') {
                        openParens--;
                    }
                }

                if(start != 0){
                    parameters.add(fullParameterString.substring(start));
                }

                if(parameters.size() == 0){
                    parameters.add(fullParameterString);
                }

                return parameters;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)`
            //        | number | functionName factor | factor `^` factor
            double parseExpression() {
                double result = parseTerm();
                for (;;) {
                    if      (eat('+')) result += parseTerm(); // addition
                    else if (eat('-')) result -= parseTerm(); // subtraction
                    else return result;
                }
            }

            CalculatorResult parseMultipleArgumentExpressions(){
                CalculatorResult result = new CalculatorResult(0);

                String paramString = getFullParameterString();
                ArrayList<String> params = getParameters(paramString);
                if(params.size() == 0)
                    return result;

                result.value = Calculator.eval(formulaMethod, params.get(0));

                for(int i=0; i<params.size(); i++){
                    result.values.add(Calculator.eval(formulaMethod, params.get(i).trim()));
                }

                return result;
            }

            double parseTerm() {
                double result = parseFactor();
                for (;;) {
                    if      (eat('*')) result *= parseFactor(); // multiplication
                    else if (eat('/')) result /= parseFactor(); // division
                    else return result;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z') nextChar();
                    String func = str.substring(startPos, this.pos).toLowerCase();
                    eat('(');
                    CalculatorResult result = parseMultipleArgumentExpressions();
                    x = result.value;
                    eat(')');
                    if (func.equals("sqrt")) x = Math.sqrt(x);
                    else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
                    else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
                    else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
                    else if (func.equals("random") || func.equals("rand")) x = parseRandom(result);
                    else if (func.equals("min")) x = parseMinimum(result.values);
                    else if (func.equals("max")) x = parseMaximum(result.values);
                    else if (func.equals("abs")) x = parseAbsoluteValue(result.value);
                    else if (func.equals("ln")) x = parseNaturalLogarithm(result.value);
                    else if (func.equals("log")) x = parseLogarithm(result);
                    else if (func.equals("exp")) x = parseExp(result.value);
                    else if (func.equals("randexp")) x = parseRandomFromExponentialDistribution(result.value);
                    else if (func.equals("randg")) x = parseRandomFromGammaDistribution(result.values);
                    else if (func.equals("floor")) x = parseFloor(result.value);
                    else if (func.equals("ceil")) x = parseCeil(result.value);
                    else if (func.equals("mod")) x = parseModulo(result.values);
                }
                else{
                    throw new RuntimeException("Unexpected: " + (char)ch + " at index " + pos);
                }

                if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation
                return x;
            }

            double parseModulo(List<Double> values) {
                if (values.size() != 2) throw new IllegalArgumentException("mod() must take 2 arguments");

                double number = values.get(0);
                double modulus = values.get(1);

                return number % modulus;
            }

            double parseCeil(double value) {
                return Math.ceil(value);
            }

            double parseFloor(double value) {
                return Math.floor(value);
            }

            //EXPERIMENTAL!
            double parseRandomFromGammaDistribution(List<Double> values) {
                if (values.size() != 2) throw new IllegalArgumentException("Gamma Distribution must take 2 arguments");

                int n = (int) (double) values.get(0);
                double lambda = values.get(1);

                if (lambda <= 0) throw new IllegalArgumentException("Lambda must be a positive number");

                double sum = 0.0;

                for (int i = 0; i < n; i++) {
                    sum += parseRandomFromExponentialDistribution(lambda);
                }

                return sum;
            }

            //EXPERIMENTAL!
            double parseRandomFromExponentialDistribution(double lambda) {
                if (lambda <= 0) throw new IllegalArgumentException("Lambda must be a positive number");
                return - (1 / lambda) * parseNaturalLogarithm(random.nextDouble());
            }

            double parseExp(double value) {
                return Math.exp(value);
            }

            double parseLogarithm(CalculatorResult result) {
                double numberOf;
                double base;

                if(result.values.size() == 1) {
                    numberOf = result.value;
                    base = 10.0;
                }
                else if(result.values.size() == 2){
                    numberOf = result.values.get(0);
                    base = result.values.get(1);
                }
                else
                    return 0;

                if (base == 1.0) throw new IllegalArgumentException("The logarithm base can't be 1");

                return parseNaturalLogarithm(numberOf) / parseNaturalLogarithm(base);
            }

            double parseNaturalLogarithm(double value) {
                if (value <= 0) throw new IllegalArgumentException("The arguments in ln must be positive number");
                return Math.log(value);
            }

            double parseAbsoluteValue(double value) {
                return Math.abs(value);
            }

            double parseRandom(CalculatorResult result){
                if(result.values.size() == 1) {
                    int range = (int) result.value;
                    if(range <= 0)
                        return 0;

                    return random.nextInt((int) result.value);
                }
                else if(result.values.size() == 2){
                    int a = (int) (double) result.values.get(0);
                    int b = (int) (double) result.values.get(1);
                    int min = Math.min(a, b);
                    int max = Math.max(a, b);
                    return random.nextInt(max-min) + min;
                }
                return 0;
            }

            double parseMinimum(List<Double> args){
                if(args.size() == 0)
                    return 0;

                Double min = args.get(0);
                for(int i=1; i<args.size(); i++){
                    min = Math.min(min, args.get(i));
                }

                return min;
            }

            double parseMaximum(List<Double> args){
                if(args.size() == 0)
                    return 0;

                Double max = args.get(0);
                for(int i=1; i<args.size(); i++){
                    max = Math.max(max, args.get(i));
                }

                return max;
            }
        }.parse();
    }
}