package me.lorinth.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

/**
 * Server logs formatting methods
 */
public abstract class OutputHandler {
    private ChatColor infoColor;
    private ChatColor errorColor;
    private ChatColor commandColor;
    private ChatColor highLightColor;
    private String infoPrefix;
    private String errorPrefix;
    private ConsoleCommandSender console = Bukkit.getConsoleSender();

    /**
     * Constructor
     * @param pluginName Plugin name
     * @param infoColor ChatColor for info outputs
     * @param errorColor ChatColor for error outputs
     * @param commandColor ChatColor for command outputs
     * @param highLightColor ChatColor for highlighted outputs
     */
    public OutputHandler(String pluginName, ChatColor infoColor, ChatColor errorColor,
                         ChatColor commandColor, ChatColor highLightColor){
        String consolePrefix = "[" + pluginName + "] : ";
        this.infoPrefix = infoColor + consolePrefix;
        this.errorPrefix = errorColor + "[Error]" + infoPrefix + errorColor;
        this.infoColor = infoColor;
        this.errorColor = errorColor;
        this.commandColor = commandColor;
        this.highLightColor = highLightColor;
    }

    /**
     * Get info color
     * @return ChatColor used for formatting information outputs
     */
    public ChatColor getInfo(){
        return infoColor;
    }

    /**
     * Get error color
     * @return ChatColor used for formatting error outputs
     */
    public ChatColor getError(){
        return errorColor;
    }

    /**
     * Get command color
     * @return ChatColor used for formatting command outputs
     */
    public ChatColor getCommand(){
        return commandColor;
    }

    /**
     * Get highlighted color
     * @return ChatColor used for formatting highlighted outputs
     */
    public ChatColor getHighLight(){
        return highLightColor;
    }

    /**
     * Log info message without console prefix ([pluginName]) in console
     * @param message String representing info message to log
     */
    public void printRawInfo(String message){ console.sendMessage(infoColor + message); }

    /**
     * Log error message without console prefix ([pluginName]) in console
     * @param message String representing error message to log
     */
    public void printRawError(String message){ console.sendMessage(errorColor + message); }

    /**
     * Log info message with console prefix ([pluginName]) in console
     * @param message String representing info message to log
     */
    public void printInfo(String message){
        console.sendMessage(infoPrefix + message);
    }

    /**
     * Log error message with console prefix ([pluginName]) in console
     * @param message String representing error message to log
     */
    public void printError(String message){
        console.sendMessage(this.errorPrefix + message);
    }

    /**
     * Log error message with exception stack trace in console
     * @param message String representing error message to log
     * @param exception Thrown exception
     */
    public void printException(String message, Exception exception){
        printError(message);
        exception.printStackTrace();
    }

    /**
     * Log info message in specified CommandSender with console prefix ([pluginName])
     * @param sender CommandSender representing entity that should log message
     * @param message String representing info message to log
     */
    public void printInfo(CommandSender sender, String message){
        sender.sendMessage(infoPrefix + message);
    }

    /**
     * Log command output message in specified CommandSender
     * @param sender CommandSender representing entity that should log message
     * @param message String representing command output message to log
     */
    public void printCommand(CommandSender sender, String message){
        sender.sendMessage(commandColor + message);
    }

    /**
     * Log error message in specified CommandSender with console prefix ([pluginName])
     * @param sender CommandSender representing entity that should log message
     * @param message String representing error message to log
     */
    public void printError(CommandSender sender, String message){
        sender.sendMessage(errorPrefix + message);
    }

    /**
     * Log info message in specified CommandSender without console prefix ([pluginName])
     * @param sender CommandSender representing entity that should log message
     * @param message String representing info message to log
     */
    public void printRawInfo(CommandSender sender, String message){
        sender.sendMessage(infoColor + message);
    }

    /**
     * Log error message in specified CommandSender without console prefix ([pluginName])
     * @param sender CommandSender representing entity that should log message
     * @param message String representing error message to log
     */
    public void printRawError(CommandSender sender, String message){
        sender.sendMessage(errorColor + message);
    }

    /**
     * Log empty lines in specified CommandSender
     * @param sender CommandSender representing entity that should log message
     * @param lines Integer representing quantity of empty lines to log
     */
    public void printWhiteSpace(CommandSender sender, int lines){
        for(int i=0; i<lines; i++)
            sender.sendMessage("");
    }
}
