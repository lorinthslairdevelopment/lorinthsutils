package me.lorinth.utils;

import com.sucy.enchant.EnchantmentAPI;
import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import su.nightexpress.goldenenchants.GoldenEnchants;
import su.nightexpress.goldenenchants.manager.EnchantRegister;
import su.nightexpress.goldenenchants.manager.enchants.GoldenEnchant;

import java.lang.reflect.Field;

public class EnchantmentHelper {

    public static Boolean GoldenEnchantsFound = null;
    public static Object GoldenEnchantsManager = null;
    public static Boolean EnchantmentApiFound = null;

    public static Object getEnchantmentByName(String enchantName){
        if(GoldenEnchantsFound == null){
            hasGoldenEnchants();
        }
        if(EnchantmentApiFound == null){
            hasEnchantApi();
        }

        Object enchant = getEnchantmentByFriendlyName(enchantName);
        if(enchant == null && GoldenEnchantsFound){
            enchant = getGoldenEnchantByName(enchantName);
        }
        if(enchant == null && EnchantmentApiFound){
            enchant = EnchantmentAPI.getEnchantment(enchantName);
        }

        return enchant;
    }

    private static GoldenEnchant getGoldenEnchantByName(String enchantName){
        try{
            Field[] declaredFields = EnchantRegister.class.getDeclaredFields();
            for (Field field : declaredFields) {
                if (java.lang.reflect.Modifier.isStatic(field.getModifiers())
                        && field.getName().equalsIgnoreCase(enchantName)) {
                    Object o = field.get(null);
                    if(o instanceof GoldenEnchant)
                        return (GoldenEnchant) o;
                }
            }
        }
        catch(Exception e){
            System.out.println("Unable to access EnchantRegister in GoldenEnchants");
        }
        return null;
    }

    private static Enchantment getEnchantmentByFriendlyName(String enchantName){
        switch(enchantName.toLowerCase().replace('_', ' ')){
            case "power":
                return Enchantment.ARROW_DAMAGE;
            case "flame":
                return Enchantment.ARROW_FIRE;
            case "infinity":
                return Enchantment.ARROW_INFINITE;
            case "punch":
                return Enchantment.ARROW_KNOCKBACK;
            case "curse of binding":
                return Enchantment.BINDING_CURSE;
            case "channeling":
                return Enchantment.CHANNELING;
            case "sharpness":
                return Enchantment.DAMAGE_ALL;
            case "bane of arthropods":
                return Enchantment.DAMAGE_ARTHROPODS;
            case "smite":
                return Enchantment.DAMAGE_UNDEAD;
            case "depth strider":
                return Enchantment.DEPTH_STRIDER;
            case "efficiency":
                return Enchantment.DIG_SPEED;
            case "unbreaking":
                return Enchantment.DURABILITY;
            case "fire aspect":
                return Enchantment.FIRE_ASPECT;
            case "frost walker":
                return Enchantment.FROST_WALKER;
            case "impaling":
                return Enchantment.IMPALING;
            case "knockback":
                return Enchantment.KNOCKBACK;
            case "fortune":
                return Enchantment.LOOT_BONUS_BLOCKS;
            case "looting":
                return Enchantment.LOOT_BONUS_MOBS;
            case "loyalty":
                return Enchantment.LOYALTY;
            case "luck of the sea":
                return Enchantment.LUCK;
            case "lure":
                return Enchantment.LURE;
            case "mending":
                return Enchantment.MENDING;
            case "multishot":
                return Enchantment.MULTISHOT;
            case "respiration":
                return Enchantment.OXYGEN;
            case "piercing":
                return Enchantment.PIERCING;
            case "protection":
                return Enchantment.PROTECTION_ENVIRONMENTAL;
            case "blast protection":
                return Enchantment.PROTECTION_EXPLOSIONS;
            case "projectile protection":
                return Enchantment.PROTECTION_PROJECTILE;
            case "feather falling":
                return Enchantment.PROTECTION_FALL;
            case "fire protection":
                return Enchantment.PROTECTION_FIRE;
            case "quick charge":
                return Enchantment.QUICK_CHARGE;
            case "riptide":
                return Enchantment.RIPTIDE;
            case "silk touch":
                return Enchantment.SILK_TOUCH;
            case "sweeping edge":
                return Enchantment.SWEEPING_EDGE;
            case "thorns":
                return Enchantment.THORNS;
            case "curse of vanishing":
                return Enchantment.VANISHING_CURSE;
            case "aqua affinity":
                return Enchantment.WATER_WORKER;
        }

        for(Enchantment e : Enchantment.values()){
            if(e.getName().equalsIgnoreCase(enchantName)){
                return e;
            }
        }
        return null;
    }

    private static void hasGoldenEnchants(){
        GoldenEnchantsFound = Bukkit.getPluginManager().getPlugin("GoldenEnchants") != null;
        if(GoldenEnchantsFound){
            GoldenEnchants goldenEnchants = (GoldenEnchants) Bukkit.getPluginManager().getPlugin("GoldenEnchants");
            GoldenEnchantsManager = goldenEnchants.getEnchantManager();
        }
    }

    private static void hasEnchantApi(){
        EnchantmentApiFound = Bukkit.getPluginManager().getPlugin("EnchantmentAPI") != null;
    }
}
