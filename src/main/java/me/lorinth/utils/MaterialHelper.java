package me.lorinth.utils;

import me.idlibrary.main.IDLibrary;
import me.lorinth.utils.objects.MaterialDurability;
import org.bukkit.Bukkit;
import org.bukkit.Material;

/**
 * Material manipulation methods
 */
public class MaterialHelper {

    /**
     * Get material with it's durability
     * @param materialValue String representing Material in format: material_name_or_id_library_index:optional_durability
     * @return MaterialDurability {@link MaterialDurability} object containing information about Material and it's durability
     */
    public static MaterialDurability getMaterial(String materialValue){
        String[] materialParts = materialValue.split(":");
        Material material;
        short durability = 0;

        if(TryParse.parseInt(materialParts[0])){
            material = useIdLibrary(Integer.parseInt(materialParts[0]));
        }
        else{
            material = Material.valueOf(materialParts[0]);
        }

        if(materialParts.length > 1)
            durability = Short.parseShort(materialParts[1]);

        return new MaterialDurability(material, durability);
    }

    /**
     * Get Material by it's id from ID-Library.
     * @param id Integer value representing id of material
     * @return Material found with given id or null if ID-Library plugin was not found.
     */
    private static Material useIdLibrary(Integer id){
        if(Bukkit.getPluginManager().getPlugin("ID-Library") != null){
            return IDLibrary.getMaterial(id.toString());
        }

        return null;
    }

}
