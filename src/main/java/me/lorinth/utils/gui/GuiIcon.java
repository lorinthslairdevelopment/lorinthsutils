package me.lorinth.utils.gui;

import me.lorinth.utils.gui.callbackTypes.Clicked;
import org.bukkit.inventory.ItemStack;

public class GuiIcon<T> {
    public ItemStack icon;
    public T metadata;
    public Clicked<T> callback;

    public GuiIcon(ItemStack icon, T metadata, Clicked<T> callback){
        this.icon = icon;
        this.metadata = metadata;
        this.callback = callback;
    }
}
