package me.lorinth.utils.gui.callbackTypes;

public interface Clicked<T> {
    boolean click(T input);
}
