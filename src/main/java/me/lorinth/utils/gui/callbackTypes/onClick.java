package me.lorinth.utils.gui.callbackTypes;

import me.lorinth.utils.gui.IconMenu;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public interface onClick {
    boolean click(Player clicker, IconMenu menu, IconMenu.Row row, int slot, ItemStack item, InventoryClickEvent event);
}
