package me.lorinth.utils.gui;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class PageMenu<T> {

    private IconMenu menu;
    private HashMap<Integer, GuiIcon<T>> content = new HashMap<>();
    private int page = 0;
    private int serverRows = -1;
    private int totalRows = 0;
    protected ItemStack up = new ItemStack(Material.YELLOW_STAINED_GLASS_PANE, 1);
    protected ItemStack down = new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1);

    public PageMenu(Plugin plugin, Player player, String menuName){
        menu = new IconMenu(plugin, menuName, 5, this::click);

        ItemMeta itemMeta = up.getItemMeta();
        itemMeta.setDisplayName(ChatColor.YELLOW + "<-- Page Up -->");
        up.setItemMeta(itemMeta);
        itemMeta = down.getItemMeta();
        itemMeta.setDisplayName(ChatColor.GREEN + "<-- Page Down -->");
        down.setItemMeta(itemMeta);
    }

    public void setContent(HashMap<Integer, GuiIcon<T>> content){
        this.content = content;
        totalRows = serverRows + content.size() / 9 + 1;
    }

    public boolean click(Player clicker, IconMenu menu, IconMenu.Row row, int slot, ItemStack item, InventoryClickEvent event) {
        if(item != null){
            if(item.isSimilar(up) && row.row == 0) {
                page -= 1;
                makePage(clicker);
            }
            else if (item.isSimilar(down) && row.row == 4) {
                page += 1;
                makePage(clicker);
            }
            else{
                int currentSlot = getClickedWarpSlot(row.row, slot);
                if (content.containsKey(currentSlot)){
                    GuiIcon<T> icon = content.get(currentSlot);
                    icon.callback.click(icon.metadata);
                }
            }
        }
        return true;
    }

    public void makePage(Player player){
        //menu.clear(player);

        int startRow = 0;
        int endRow = 5;

        if(page > 0){
            for(int i=0; i<9; i++)
                menu.addButton(menu.getRow(0), i, up.clone());
            startRow++;
        }

        //If there is a next page, add a bottom bar
        if(totalRows > page * 3 + 4){
            for(int i=0; i<9; i++)
                menu.addButton(menu.getRow(4), i, down.clone());
            endRow--;
        }

        for(int currentRow = startRow; currentRow<endRow; currentRow++) {
            for(int slot=0; slot<9; slot++) {
                int contentSlot = getContentSlot(currentRow, slot);
                if (contentSlot < content.size()){
                    menu.addButton(menu.getRow(currentRow), slot, content.get(contentSlot));
                }
            }
        }

        menu.open(player);
    }

    private int getCurrentRow(int currentRow){
        return currentRow + page*3; //3 in each following page
    }

    private int getClickedWarpSlot(int row, int slot){
        return (getCurrentRow(row) - (serverRows + 1)) * 9 + slot;
    }

    private int getContentSlot(int row, int slot){
        return getCurrentRow(row) * 9 + slot;
    }

}
