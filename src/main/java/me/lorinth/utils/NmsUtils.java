package me.lorinth.utils;

import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

public class NmsUtils {

    public static Item addOwner(Plugin plugin, Item item, Player player) {
        PersistentDataContainer container = item.getPersistentDataContainer();
        if(container != null)
            container.set(new NamespacedKey(plugin, "Owner"), PersistentDataType.STRING, player.getUniqueId().toString());

        return item;
    }

    public static ItemStack addTag(Plugin plugin, ItemStack item, String key, String value) {
        ItemMeta meta = item.getItemMeta();

        PersistentDataContainer container = meta.getPersistentDataContainer();
        if(container != null)
            container.set(new NamespacedKey(plugin, key), PersistentDataType.STRING, value);

        item.setItemMeta(meta);
        return item;
    }


    public static ItemMeta addTag(Plugin plugin, ItemMeta meta, String key, String value) {
        PersistentDataContainer container = meta.getPersistentDataContainer();
        if(container != null)
            container.set(new NamespacedKey(plugin, key), PersistentDataType.STRING, value);

        return meta;
    }

    public static String getTag(Plugin plugin, ItemStack item, String key){
        ItemMeta meta = item.getItemMeta();

        PersistentDataContainer container = meta.getPersistentDataContainer();
        if(container != null)
            return container.get(new NamespacedKey(plugin, key), PersistentDataType.STRING);
        return null;
    }

    public static String getTag(Plugin plugin, ItemMeta meta, String key){
        PersistentDataContainer container = meta.getPersistentDataContainer();
        if(container != null)
            return container.get(new NamespacedKey(plugin, key), PersistentDataType.STRING);
        return null;
    }

    public static ItemStack removeTag(Plugin plugin, ItemStack item, String key){
        ItemMeta meta = item.getItemMeta();

        PersistentDataContainer container = meta.getPersistentDataContainer();
        if(container != null)
            container.remove(new NamespacedKey(plugin, key));

        item.setItemMeta(meta);
        return item;
    }

    public static ItemMeta removeTag(Plugin plugin, ItemMeta meta, String key){
        PersistentDataContainer container = meta.getPersistentDataContainer();
        if(container != null)
            container.remove(new NamespacedKey(plugin, key));

        return meta;
    }

    public static void RemoveDamageAttribute(ItemStack i){
        ItemMeta meta = i.getItemMeta();

        if(meta != null && !meta.hasItemFlag(ItemFlag.HIDE_ATTRIBUTES)) {
            meta.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, new AttributeModifier("clear", 0, AttributeModifier.Operation.MULTIPLY_SCALAR_1));
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        }

        i.setItemMeta(meta);
    }

    public static void RemoveArmorAttribute(ItemStack i){
        ItemMeta meta = i.getItemMeta();

        if(meta != null && !meta.hasItemFlag(ItemFlag.HIDE_ATTRIBUTES)) {
            meta.addAttributeModifier(Attribute.GENERIC_ARMOR, new AttributeModifier("clear", 0, AttributeModifier.Operation.MULTIPLY_SCALAR_1));
            meta.addAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS, new AttributeModifier("clear", 0, AttributeModifier.Operation.MULTIPLY_SCALAR_1));
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        }

        i.setItemMeta(meta);
    }

}
