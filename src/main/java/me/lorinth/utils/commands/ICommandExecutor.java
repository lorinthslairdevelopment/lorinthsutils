package me.lorinth.utils.commands;

import org.bukkit.command.CommandSender;

public interface ICommandExecutor {

    void execute(CommandSender commandSender, String[] args);

    void sendHelp(CommandSender commandSender);

}
