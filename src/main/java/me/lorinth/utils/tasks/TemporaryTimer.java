package me.lorinth.utils.tasks;

import org.bukkit.scheduler.BukkitRunnable;

public abstract class TemporaryTimer extends BukkitRunnable {

    private double value = 0.0;
    private double maxValue = 1.0;
    private double step = 1.0;

    public TemporaryTimer(double maxValue){
        this.maxValue = maxValue;
    }

    public TemporaryTimer(double maxValue, double step){
        this.maxValue = maxValue;
        this.step = step;
    }

    public abstract void tick();

    @Override
    public void run(){
        tick();

        value += step;
        if(value >= maxValue){
            this.cancel();
        }
    }

    public double getValue(){
        return value;
    }

}
