package me.lorinth.utils;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.regex.Pattern;

public class NMSUtil {

    private static String nmsVersion;
    private static Class<?> craftMetaItemClass;
    private static Class<?> nbtTagCompound;

    private static Field unhandledTagsField;

    private static Method setString;
    private static Method getString;

    static {
        nmsVersion = getNMSVersion();

        craftMetaItemClass = getOBCClass("inventory.CraftMetaItem");
        nbtTagCompound = getNMSClass("NBTTagCompound");

        try {
            if (nbtTagCompound != null) {
                setString = nbtTagCompound.getMethod("setString", String.class, String.class);
                getString = nbtTagCompound.getMethod("getString", String.class);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        try {
            if (craftMetaItemClass != null) {
                unhandledTagsField = craftMetaItemClass.getDeclaredField("unhandledTags");
                unhandledTagsField.setAccessible(true);
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

    }

    public static Class<?> getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server." + nmsVersion + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Class<?> getOBCClass(String name) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + nmsVersion + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getNMSVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().split(Pattern.quote("."))[3];
    }

    public static void setNBTString(ItemMeta meta, String nbtKey, String valueKey, String value) {
        try {
            Object tag = nbtTagCompound.newInstance();
            setString.invoke(tag, valueKey, value);
            ((Map<String, Object>)unhandledTagsField.get(meta)).put(nbtKey, tag);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static String getNBTString(ItemMeta meta, String nbtKey, String valueKey) {
        try {
            Object tag = ((Map<String, Object>)unhandledTagsField.get(meta)).get(nbtKey);

            if (tag == null)
                return null;

            return (String) getString.invoke(tag, valueKey);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }


    /**
     * Converts an {@link org.bukkit.inventory.ItemStack} to a Json string
     * for sending with {@link net.md_5.bungee.api.chat.BaseComponent}'s.
     *
     * @param itemStack the item to convert
     * @return the Json string representation of the item
     */
    public static String convertItemStackToJson(ItemStack itemStack, OutputHandler outputHandler) {
        try{
            // ItemStack methods to get a net.minecraft.server.ItemStack object for serialization
            Class<?> craftItemStackClazz = NMSUtil.getOBCClass("inventory.CraftItemStack");
            Method asNMSCopyMethod = craftItemStackClazz.getMethod("asNMSCopy", ItemStack.class);

            // NMS Method to serialize a net.minecraft.server.ItemStack to a valid Json string
            Class<?> nmsItemStackClazz = getNMSClass("ItemStack");
            Class<?> nbtTagCompoundClazz = getNMSClass("NBTTagCompound");
            Method saveNmsItemStackMethod = nmsItemStackClazz.getMethod("save", nbtTagCompoundClazz);

            Object nmsNbtTagCompoundObj; // This will just be an empty NBTTagCompound instance to invoke the saveNms method
            Object nmsItemStackObj; // This is the net.minecraft.server.ItemStack object received from the asNMSCopy method
            Object itemAsJsonObject; // This is the net.minecraft.server.ItemStack after being put through saveNmsItem method

            nmsNbtTagCompoundObj = nbtTagCompoundClazz.newInstance();
            nmsItemStackObj = asNMSCopyMethod.invoke(null, itemStack);
            itemAsJsonObject = saveNmsItemStackMethod.invoke(nmsItemStackObj, nmsNbtTagCompoundObj);
            return itemAsJsonObject.toString();
        } catch (Exception ex) {
            outputHandler.printException("Failed to serialize itemstack to nms item", ex);
            return null;
        }

        // Return a string representation of the serialized object
    }

}
