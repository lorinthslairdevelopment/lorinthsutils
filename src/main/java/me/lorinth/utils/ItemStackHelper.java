package me.lorinth.utils;

import com.google.common.collect.Multimap;
import com.ssomar.executableitems.api.ExecutableItemsAPI;
import com.sucy.enchant.api.CustomEnchantment;
import me.lorinth.utils.objects.MaterialDurability;
import net.Indyuce.mmoitems.MMOItems;
import net.Indyuce.mmoitems.api.Type;
import net.Indyuce.mmoitems.manager.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import su.nightexpress.goldenenchants.manager.EnchantManager;
import su.nightexpress.goldenenchants.manager.enchants.GoldenEnchant;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class ItemStackHelper {

    public static ItemStack makeItemFromConfig(FileConfiguration config, String rootPath, String itemId, String fileName, OutputHandler outputHandler){
        return makeItemFromConfig(config, rootPath + "." + itemId, fileName, outputHandler);
    }

    public static ItemStack makeItemFromConfig(FileConfiguration config, String path, String fileName, OutputHandler outputHandler){
        try{
            Set<String> options = config.getConfigurationSection(path).getKeys(false);
            for(String option : options){
                if(option.equalsIgnoreCase("mmoitem") || option.equalsIgnoreCase("mmoitems")){
                    String info = config.getString(path + "." + option);
                    if(info == null) {
                        outputHandler.printError(fileName + ", failed to load mmoitem at " + outputHandler.getHighLight() + path + ".Id");
                        return null;
                    }

                    ItemStack item = getMmoItem(info, outputHandler);
                    if (item == null){
                        outputHandler.printError(fileName + ", invalid mmo item with id, " + info);
                    }
                    return item;
                }
                if (option.equalsIgnoreCase("ei") || option.equalsIgnoreCase("execitem")){
                    String info = config.getString(path + "." + option);
                    if(info == null) {
                        outputHandler.printError(fileName + ", failed to load execitem at " + outputHandler.getHighLight() + path + ".Id");
                        return null;
                    }

                    ItemStack item = getExecutableItem(info, outputHandler);
                    if (item == null){
                        outputHandler.printError(fileName + ", invalid executable item with id, " + info);
                    }
                    return item;
                }
            }

            String materialValue = config.getString(path + ".Id");
            Material material;
            int count = 1;
            short durability;

            try{
                MaterialDurability md = MaterialHelper.getMaterial(materialValue);
                material = md.getMaterial();
                durability = md.getDurability();

                if(material == null)
                    throw new Exception("Invalid Material");
            }
            catch(Exception e){
                outputHandler.printError(fileName + ", invalid material/durability at " + outputHandler.getHighLight() + path + ".Id");
                return null;
            }

            if(ConfigHelper.ConfigContainsPath(config, path + ".Count")){
                count = config.getInt(path + ".Count");
            }

            ItemStack item = new ItemStack(material, count, durability);
            ItemMeta meta = item.getItemMeta();
            if(meta == null){
                return item;
            }

            if(ConfigHelper.ConfigContainsPath(config, path + ".DisplayName")){
                meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', config.getString(path + ".DisplayName")));
            }
            if(ConfigHelper.ConfigContainsPath(config, path + ".Enchantments")){
                item.setItemMeta(meta);
                for(String enchantName : config.getConfigurationSection(path + ".Enchantments").getKeys(false)){
                    Object enchant = EnchantmentHelper.getEnchantmentByName(enchantName);
                    int level = config.getInt(path + ".Enchantments." + enchantName);
                    if(enchant == null){
                        outputHandler.printError(fileName + ", invalid Enchantment, " + outputHandler.getHighLight() + enchantName + outputHandler.getError() + ", at " + outputHandler.getHighLight() + path + ".Enchantments");
                    } else if(EnchantmentHelper.GoldenEnchantsFound && enchant instanceof GoldenEnchant){
                        ((EnchantManager) EnchantmentHelper.GoldenEnchantsManager).addEnchant(item, (GoldenEnchant) enchant, level);
                    } else if(EnchantmentHelper.EnchantmentApiFound && enchant instanceof CustomEnchantment){
                        item = ((CustomEnchantment) enchant).addToItem(item, level);
                    } else if(enchant instanceof Enchantment){
                        item.addUnsafeEnchantment((Enchantment) enchant, level);
                    }
                }
                meta = item.getItemMeta();
            }
            if(ConfigHelper.ConfigContainsPath(config, path + ".Lore")){
                List<String> lore = new ArrayList<>();
                for(String line : config.getStringList(path + ".Lore")){
                    lore.add(ChatColor.translateAlternateColorCodes('&', line));
                }
                meta.setLore(lore);
            }
            if(ConfigHelper.ConfigContainsPath(config, path + ".ItemFlags")){
                for(String value : config.getStringList(path + ".ItemFlags")){
                    if(TryParse.parseEnum(ItemFlag.class, value))
                        meta.addItemFlags(ItemFlag.valueOf(value));
                    else
                        outputHandler.printError(fileName + ", invalid ItemFlag, " + outputHandler.getHighLight() + value + outputHandler.getError() + ", at " + outputHandler.getHighLight() + path + ".ItemFlags");
                }
            }

            if(ConfigHelper.ConfigContainsPath(config, path + ".CustomModelData")){
                meta.setCustomModelData(config.getInt(path + ".CustomModelData"));
            }

            int subVersion = NmsHelper.getSimpleVersion();
            if(subVersion >= 13){
                Multimap<Attribute, AttributeModifier> attributeMap = meta.getAttributeModifiers();
                if(ConfigHelper.ConfigContainsPath(config, path + ".Attributes")){
                    for(String key : config.getConfigurationSection(path + ".Attributes").getKeys(false)){
                        if(!TryParse.parseEnum(Attribute.class, key)){
                            outputHandler.printError("Failed to parse invalid minecraft attribute, " + outputHandler.getHighLight() + key + outputHandler.getError() + ", in item with id " + path);
                            continue;
                        }

                        Attribute attribute = Attribute.valueOf(key);
                        AttributeModifier.Operation operation;
                        if(!ConfigHelper.ConfigContainsPath(config,path + ".Attributes." + key + ".Operation" )){
                            operation = AttributeModifier.Operation.ADD_NUMBER;
                        }
                        else {
                            String operationString = config.getString(path + ".Attributes." + key + ".Operation");
                            if(!TryParse.parseEnum(AttributeModifier.Operation.class, operationString)){
                                outputHandler.printError("Failed to parse invalid minecraft attribute operation, " + outputHandler.getHighLight() + operationString + outputHandler.getError() + ", in item with id " + path);
                                continue;
                            }
                            operation = AttributeModifier.Operation.valueOf(operationString);
                        }

                        double amount = 0;
                        if(!ConfigHelper.ConfigContainsPath(config,path + ".Attributes." + key + ".Value" )){
                            outputHandler.printError("Failed to location attribute value in item with id " + path);
                            continue;
                        }
                        else{
                            amount = config.getDouble(path + ".Attributes." + key + ".Value");
                        }

                        //TODO: Change the RpgMobs_ modifier key
                        attributeMap.put(attribute, new AttributeModifier("RpgMobs_" + attribute.name(), amount, operation));
                    }
                }

            }
            try{
                if(ConfigHelper.ConfigContainsPath(config, path + ".NBTTags")){
                    for(String nbtTag : config.getConfigurationSection(path + ".NBTTags").getKeys(false)){
                        for(String valueKey : config.getConfigurationSection(path + ".NBTTags." + nbtTag).getKeys(false)){
                            NMSUtil.setNBTString(meta, nbtTag, valueKey, config.getString(path + ".NBTTags." + nbtTag + "." + valueKey));
                        }
                    }
                }
            } catch(Exception e){
                outputHandler.printException("Issue loading NBTTags for item, " + outputHandler.getHighLight() + meta.getDisplayName(), e);
            }

            item.setItemMeta(meta);
            return item;
        }
        catch(Exception e){
            outputHandler.printError("Error loading item in file, "
                    + outputHandler.getHighLight() + fileName
                    + outputHandler.getError() + " at path, "
                    + outputHandler.getHighLight() + path);

            return null;
        }
    }

    private static ItemStack getMmoItem(String info, OutputHandler outputHandler){
        String[] args = info.split(Pattern.quote(" "));
        String mmoItemType = args[0];
        String mmoItemId = args[1];

        if(Bukkit.getPluginManager().getPlugin("MMOItems") == null){
            outputHandler.printError("MMOItems plugin not found, cannot load mmoitems in loot.yml");
            return null;
        }

        Type type = MMOItems.plugin.getTypes().get(mmoItemType);
        if(type == null){
            outputHandler.printError("Invalid mmoitems type in: " + outputHandler.getHighLight() + info);
            return null;
        }

        ItemManager itemManager = MMOItems.plugin.getItems();
        if(itemManager == null){
            outputHandler.printError("Unable to find ItemManager in MMOItems plugin... Did it load properly?");
            return null;
        }

        ItemStack item = itemManager.getItem(type, mmoItemId);
        if(item == null){
            outputHandler.printError("Invalid mmoitems id in: " + outputHandler.getHighLight() + info);
            return null;
        }

        return item;
    }

    private static ItemStack getExecutableItem(String info, OutputHandler outputHandler) {
        String[] args = info.split(Pattern.quote(" "));
        String id = args[0];

        if (Bukkit.getPluginManager().getPlugin("ExecutableItems") == null) {
            outputHandler.printError("ExecutableItems plugin not found, cannot load execitem in loot.yml");
            return null;
        }

        return ExecutableItemsAPI.getExecutableItem(id);
    }

}
