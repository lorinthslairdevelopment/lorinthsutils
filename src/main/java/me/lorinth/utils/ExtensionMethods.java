package me.lorinth.utils;

import java.util.List;

public class ExtensionMethods {

    public static List addOrSet(List list, int index, Object item){
        if(list.size() > index){
            list.set(index, item);
        }
        else{
            while(list.size() <= index){
                list.add(null);
            }
            list.add(item);
        }
        return list;
    }

    public static List setSize(List list, int size){
        return list.subList(0, Math.min(list.size(), size));
    }

}
