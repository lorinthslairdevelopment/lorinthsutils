package me.lorinth.utils;

import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public class EquipmentSlotHelper {
    private static List<Material> materials;

    public static List<Material> getMaterials(String equipmentSlot) {

        switch (equipmentSlot.toLowerCase()) {
            case "weapon":
                materials = new ArrayList<Material>(){{
                    add(Material.WOODEN_SWORD);
                    add(Material.WOODEN_AXE);
                    add(Material.STONE_SWORD);
                    add(Material.STONE_AXE);
                    add(Material.IRON_SWORD);
                    add(Material.IRON_AXE);
                    add(Material.GOLDEN_SWORD);
                    add(Material.GOLDEN_AXE);
                    add(Material.DIAMOND_SWORD);
                    add(Material.DIAMOND_AXE);
                    add(Material.BOW);
                    add(Material.CROSSBOW);
                    add(Material.TRIDENT);
                }};
                break;
            case "shield":
                materials = new ArrayList<Material>(){{
                    add(Material.SHIELD);
                }};
                break;
            case "helmet":
                materials = new ArrayList<Material>(){{
                    add(Material.LEATHER_HELMET);
                    add(Material.TURTLE_HELMET);
                    add(Material.CHAINMAIL_HELMET);
                    add(Material.IRON_HELMET);
                    add(Material.GOLDEN_HELMET);
                    add(Material.DIAMOND_HELMET);
                }};
                break;
            case "chestplate":
                materials = new ArrayList<Material>(){{
                    add(Material.LEATHER_CHESTPLATE);
                    add(Material.CHAINMAIL_CHESTPLATE);
                    add(Material.IRON_CHESTPLATE);
                    add(Material.GOLDEN_CHESTPLATE);
                    add(Material.DIAMOND_CHESTPLATE);
                }};
                break;
            case "leggings":
                materials = new ArrayList<Material>(){{
                    add(Material.LEATHER_LEGGINGS);
                    add(Material.CHAINMAIL_LEGGINGS);
                    add(Material.IRON_LEGGINGS);
                    add(Material.GOLDEN_LEGGINGS);
                    add(Material.DIAMOND_LEGGINGS);
                }};
                break;
            case "boots":
                materials = new ArrayList<Material>(){{
                    add(Material.LEATHER_BOOTS);
                    add(Material.CHAINMAIL_BOOTS);
                    add(Material.IRON_BOOTS);
                    add(Material.GOLDEN_BOOTS);
                    add(Material.DIAMOND_BOOTS);
                }};
                break;
        }

        return materials;
    }
}
