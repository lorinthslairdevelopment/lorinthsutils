package me.lorinth.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Plugin resource manipulation methods
 */
public class ResourceHelper {

    /**
     * Copy resource content into newly created file
     * @param in InputStream representing resource file data stream
     * @param file File object representing resource file to create
     */
    public static void copy(InputStream in, File file) {
        if(file.exists())
            return;
        if(!file.exists())
            file.getParentFile().mkdirs();

        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
