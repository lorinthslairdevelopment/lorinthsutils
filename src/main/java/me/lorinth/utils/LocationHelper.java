package me.lorinth.utils;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;

import java.util.Random;

public class LocationHelper {

    private static Random random = new Random();

    public static boolean equal(Location a, Location b){
        return a.getWorld().getName().equalsIgnoreCase(b.getWorld().getName()) &&
                a.getBlockX() == b.getBlockX() &&
                a.getBlockY() == b.getBlockY() &&
                a.getBlockZ() == b.getBlockZ();
    }

    public static boolean LocationPartOfCircle(Location location, Location origin, double radius){
        return LocationPartOfCircle(location, origin.getX(), origin.getZ(), radius);
    }

    public static boolean LocationPartOfCircle(Location location, double originXCord, double originZCord, double radius){
        //(x - originX)^2 + (y - originY)^2 <= r^2
        return Math.pow(location.getX() - originXCord, 2) + Math.pow(location.getZ() - originZCord, 2) <= Math.pow(radius, 2);
    }

    public static boolean LocationPartOfRing(Location location,
                                             Location origin,
                                             double radiusOfFirstCircle,
                                             double radiusOfSecondCircle) {

        //Is inside first circle but outside second one
        return LocationPartOfCircle(location, origin, radiusOfFirstCircle) &&
                !LocationPartOfCircle(location, origin, radiusOfSecondCircle);
    }

    public static Location GetSafeLocationWithinRadiusOfLocation(
            Location origin,
            int radiusXZ,
            int radiusY
    ){
        return GetSafeLocationWithinRadiusOfLocation(origin, radiusXZ, radiusY, true, false);
    }

    public static Location GetSafeLocationWithinRadiusOfLocation(
        Location origin,
        int radiusXZ,
        int radiusY,
        boolean yMod,
        boolean onSurface
    ){
        if (radiusXZ == 0){
            radiusXZ = 1;
        }
        if (radiusY == 0){
            radiusY = 1;
        }

        double x = origin.getX() - radiusXZ + random.nextInt(radiusXZ * 2);
        double z = origin.getZ() - radiusXZ + random.nextInt(radiusXZ * 2);
        double y;

        if (yMod) {
            y = origin.getY() - radiusY + random.nextInt(radiusY * 2);
        } else {
            y = origin.getY() + random.nextInt(radiusY);
        }
        Location loc = new Location(origin.getWorld(), x, y, z);
        if (loc.getBlock().getType().isSolid()) {
            int j = 10;
            while (loc.getBlock().getType().isSolid()) {
                x = origin.getX() - radiusXZ + random.nextInt(radiusXZ * 2);
                z = origin.getZ() - radiusXZ + random.nextInt(radiusXZ * 2);
                if (yMod) {
                    y = origin.getY() - radiusY + random.nextInt(radiusY * 2);
                } else {
                    y = origin.getY() + random.nextInt(radiusY);
                }
                loc = new Location(origin.getWorld(), x, y, z);
                j--;
                if (j == 0) {
                    loc = new Location(origin.getWorld(), origin.getX(), origin.getY() + 1.0D, origin.getZ());
                    break;
                }
            }
        }
        if (onSurface && !loc.getBlock().getRelative(BlockFace.DOWN).getType().isSolid()) {
            int highestY = loc.getWorld().getHighestBlockYAt(loc);
            if (highestY <= loc.getY()) {
                loc.setY((highestY + 1));
            } else {
                int j = 10;
                while (!loc.getBlock().getRelative(BlockFace.DOWN).getType().isSolid()) {
                    if (j == 0) {
                        loc = new Location(origin.getWorld(), origin.getX(), origin.getY() + 1.0D, origin.getZ());
                        break;
                    }
                    loc.setY(loc.getY() - 1.0D);
                    j--;
                }
            }
        }
        return loc;
    }

    //Cone
    /*
    (Mathematical - origin (0,0,0))
    x^2 + y^2 <= (z*r/h)^2
    &&
    0 <= z <= h
     */
}
