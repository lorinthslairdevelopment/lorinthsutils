package me.lorinth.utils.holograms;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class HologramManager
{
    public static void setHoloTime(Plugin plugin, Player paramPlayer, List<String> listOfString, Location paramLocation, int duration, double height)
    {
        String[] paramArrayOfString = new String[listOfString.size()];
        paramArrayOfString = listOfString.toArray(paramArrayOfString);
        setHoloTime(plugin, paramPlayer, paramArrayOfString, paramLocation, duration, height);
    }

    public static void setHoloTime(Plugin plugin, Player paramPlayer, String[] paramArrayOfString, Location paramLocation, int duration, double height)
    {
        VersionManager.setHoloTime(plugin, paramPlayer, paramArrayOfString, paramLocation, duration, height);
    }

    public static void setGlobalHologram(Plugin plugin, List<String> listOfString, Location location, int duration, double height, double radius){
        setGlobalHologram(plugin, listOfString, location, duration, height, radius, null);
    }

    public static void setGlobalHologram(Plugin plugin, String[] paramArrayOfString, Location location, int duration, double height, double radius){
        setGlobalHologram(plugin, paramArrayOfString, location, duration, height, radius, null);
    }

    public static void setGlobalHologram(Plugin plugin, List<String> listOfString, Location location, int duration, double height, double radius, Player except){
        String[] paramArrayOfString = new String[listOfString.size()];
        paramArrayOfString = listOfString.toArray(paramArrayOfString);
        setGlobalHologram(plugin, paramArrayOfString, location, duration, height, radius, except);
    }

    public static void setGlobalHologram(Plugin plugin, String[] paramArrayOfString, Location location, int duration, double height, double radius, Player except){
        for(Player player : location.getWorld().getPlayers()){
            if (player == except){
                continue;
            }

            if(player.getLocation().distanceSquared(location) < (radius * radius))
                setHoloTime(plugin, player, paramArrayOfString, location, duration, height);
        }
    }
}

