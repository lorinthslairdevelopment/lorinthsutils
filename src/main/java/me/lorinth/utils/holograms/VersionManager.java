package me.lorinth.utils.holograms;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class VersionManager {

    public static void setHoloTime(Plugin plugin, Player paramPlayer, String[] paramArrayOfString, Location paramLocation, int duration, double height)
    {
        Object localObject;
        switch(v()){
            case "v1_8_R3":
                localObject = new PluginHologram_V1_8_R3(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_8_R3)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_9_R2":
                localObject = new PluginHologram_V1_9_R2(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_9_R2)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_10_R1":
                localObject = new PluginHologram_V1_10_R1(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_10_R1)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_11_R1":
                localObject = new PluginHologram_V1_11_R1(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_11_R1)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_12_R1":
                localObject = new PluginHologram_V1_12_R1(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_12_R1)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_13_R2":
                localObject = new PluginHologram_V1_13_R2(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_13_R2)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_14_R1":
                localObject = new PluginHologram_V1_14_R1(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_14_R1)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_15_R1":
                localObject = new PluginHologram_V1_15_R1(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_15_R1)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_16_R1":
                localObject = new PluginHologram_V1_16_R1(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_16_R1)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_16_R2":
                localObject = new PluginHologram_V1_16_R2(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_16_R2)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;
            case "v1_16_R3":
                localObject = new PluginHologram_V1_16_R3(paramArrayOfString, paramLocation, height);
                ((PluginHologram_V1_16_R3)localObject).showPlayerTemp(plugin, paramPlayer, duration);
                break;

        }
    }

    private static String v()
    {
        String str1 = Bukkit.getServer().getClass().getPackage().getName();
        return str1.substring(str1.lastIndexOf('.') + 1);
    }
}
