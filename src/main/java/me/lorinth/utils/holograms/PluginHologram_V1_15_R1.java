package me.lorinth.utils.holograms;

import net.minecraft.server.v1_15_R1.ChatComponentText;
import net.minecraft.server.v1_15_R1.EntityArmorStand;
import net.minecraft.server.v1_15_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_15_R1.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_15_R1.PacketPlayOutSpawnEntityLiving;
import org.bukkit.craftbukkit.v1_15_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class PluginHologram_V1_15_R1
{
    private List<EntityArmorStand> entitylist = new ArrayList();
    private String[] Text;
    private Location location;
    private double DISTANCE = 0.25D;
    private double HEIGHT;
    private int count;

    public PluginHologram_V1_15_R1(String[] paramArrayOfString, Location paramLocation, double height)
    {
        this.HEIGHT = height;
        this.Text = paramArrayOfString;
        this.location = paramLocation;
        create();
    }

    public void showPlayerTemp(Plugin plugin, final Player paramPlayer, int paramInt)
    {
        showPlayer(plugin, paramPlayer);
        new BukkitRunnable()
        {
            public void run()
            {
                hidePlayer(plugin, paramPlayer);
            }
        }.runTaskLaterAsynchronously(plugin, paramInt);
    }

    public void showPlayer(Plugin plugin, final Player paramPlayer)
    {
        new BukkitRunnable()
        {
            public void run()
            {
                for (EntityArmorStand localEntityArmorStand : entitylist)
                {
                    net.minecraft.server.v1_15_R1.PacketPlayOutSpawnEntityLiving localPacketPlayOutSpawnEntityLiving = new PacketPlayOutSpawnEntityLiving(localEntityArmorStand);
                    PacketPlayOutEntityMetadata localEntityMetadata = new PacketPlayOutEntityMetadata(localEntityArmorStand.getId(), localEntityArmorStand.getDataWatcher(), true);
                    ((org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer)paramPlayer).getHandle().playerConnection.sendPacket(localPacketPlayOutSpawnEntityLiving);
                    ((org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer)paramPlayer).getHandle().playerConnection.sendPacket(localEntityMetadata);
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    public void hidePlayer(Plugin plugin, final Player paramPlayer)
    {
        new BukkitRunnable()
        {
            public void run()
            {
                for (EntityArmorStand localEntityArmorStand : entitylist)
                {
                    PacketPlayOutEntityDestroy localPacketPlayOutEntityDestroy = new PacketPlayOutEntityDestroy(localEntityArmorStand.getId());
                    ((CraftPlayer)paramPlayer).getHandle().playerConnection.sendPacket(localPacketPlayOutEntityDestroy);
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    private void create()
    {
        String[] arrayOfString;
        int k = (arrayOfString = this.Text).length;
        for (int j = 0; j < k; j++)
        {
            String str = arrayOfString[j];
            EntityArmorStand localEntityArmorStand = new EntityArmorStand(((CraftWorld)this.location.getWorld()).getHandle(), this.location.getX(), this.location.getY() + HEIGHT, this.location.getZ());
            localEntityArmorStand.setCustomName(new ChatComponentText(str));
            localEntityArmorStand.setCustomNameVisible(true);
            localEntityArmorStand.setInvisible(true);
            localEntityArmorStand.setNoGravity(true);
            localEntityArmorStand.setSmall(true);
            this.entitylist.add(localEntityArmorStand);
            this.location.subtract(0.0D, this.DISTANCE, 0.0D);
            this.count += 1;
        }
        for (int i = 0; i < this.count; i++) {
            this.location.add(0.0D, this.DISTANCE, 0.0D);
        }
        this.count = 0;
    }
}
