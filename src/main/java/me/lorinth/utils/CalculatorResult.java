package me.lorinth.utils;

import java.util.ArrayList;
import java.util.List;

public class CalculatorResult{

    double value;
    List<Double> values;

    public CalculatorResult(double value){
        this.value = value;
        this.values = new ArrayList<>();
    }

    public CalculatorResult(double value, List<Double> values){
        this.value = value;
        this.values = values == null ? new ArrayList<>() : values;
    }

    public String toString(){
        return "{value: " + this.value + ", values: " + (this.values != null ? this.values.toString() : "") + "}";
    }

}
