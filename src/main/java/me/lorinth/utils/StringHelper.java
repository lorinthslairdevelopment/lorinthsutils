package me.lorinth.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Utility String modification methods
 */
public class StringHelper {

    /**
     * Change String value to camelCase
     * @param value String value to change
     * @return Value parameter changed to camelCase notation
     */
    public static String toCamelCase(String value){
        String[] words = value.split(" ");
        String camelCase = "";

        for (int i=0; i<words.length; i++){
            String word = words[i];
            if (i==0){
                camelCase += word.toLowerCase();
            }
            else {
                camelCase += (word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase());
            }
        }

        return camelCase;
    }

    /**
     * Split a String by many String characters
     * @param value String value to split
     * @param splitBy Split by characters
     * @return List with splitted Strings
     */
    public static List<String> split(String value, String... splitBy){
        ArrayList<String> args = new ArrayList<String>();

        int startIndex = 0;
        int endIndex = 0;
        ArrayList<String> quotedValues = new ArrayList<String>();
        while (value.indexOf('"', startIndex) > -1){
            startIndex = value.indexOf('"', startIndex);
            endIndex = value.indexOf('"', startIndex+1);

            if (endIndex > startIndex){
                quotedValues.add(value.substring(startIndex, endIndex+1));
            }
            else {
                System.out.println("Didn't find closing quote for string, " + value + "starting from index " + startIndex+1);
            }

            startIndex = endIndex+1;
        }

        int paramIndex = 1;
        HashMap<String, String> params = new HashMap<String, String>();
        for (String quotedValue : quotedValues){
            String paramKey = "@p" + paramIndex++;
            value = value.replace(Pattern.quote(quotedValue), paramKey);
            params.put(paramKey, quotedValue);
        }

        if(splitBy.length > 0){
            String[] splitArgs = value.split(Pattern.quote(splitBy[0]));
            args.addAll(Arrays.asList(splitArgs));
        }

        if (splitBy.length > 1){
            for (int i=1; i<splitBy.length; i++){
                ArrayList<String> newArgs = new ArrayList<String>();
                String split = splitBy[i];

                for(String arg : args){
                    newArgs.addAll(Arrays.asList(arg.split(Pattern.quote(split))));
                }
                args = newArgs;
            }
        }

        for (int i=0; i<args.size(); i++){
            String arg = args.get(i);
            if (params.containsKey(arg)){
                args.set(i, params.get(arg));
            }
        }

        return args;
    }

    /**
     * Convert List of String to parameter HashMap of String, String pairs
     * @param list List of String arguments to change to HashMap
     * @return HashMap created based on list elements or empty HashMap when list has odd number of elements
     */
    public static HashMap<String, String> toParameterMap(List<String> list) {
        if (list.size() % 2 != 0){
            System.out.println("Cannot create a parameter mapping from, " + list.toString());
            return new HashMap<String, String>();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        for (int i = 0; i < list.size(); i+=2){
            params.put(list.get(i), list.get(i+1));
        }

        return params;
    }
}
