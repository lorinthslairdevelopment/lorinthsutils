package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;

public class CirclePreset {
    //Circle
    public static TemporaryTimer getCircle(Particle particle, Location location, double radius) {
        return getCircle(particle, location, radius, 1.0, 16, 1); //1.0 as default
    }

    public static TemporaryTimer getCircle(Particle particle, Location location, double radius, double height) {
        return getCircle(particle, location, radius, height, 16, 1); //16.0 as default
    }

    public static TemporaryTimer getCircle(Particle particle, Location location, double radius, double height, int density) {
        return getCircle(particle, location, radius, height, density, 1); //1 as default
    }

    public static TemporaryTimer getCircle(Particle particle, Location location, double radius, double height, int density, int maxTimes) {
        return new TemporaryTimer(maxTimes * density) {
            double phi = 0.0;

            @Override
            public void tick() {
                phi += 2 * Math.PI / density;

                for (double theta = 0.0; theta <= 2 * Math.PI; theta += 2 * Math.PI / density) {
                    double xCord = radius * Math.cos(theta + phi);
                    double yCord = height;
                    double zCord = radius * Math.sin(theta + phi);

                    location.add(xCord, yCord, zCord);
                    location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0);
                    location.subtract(xCord, yCord, zCord);
                }
            }
        };
    }

    public static TemporaryTimer getCircle(Particle particle, Location location, double radius, double height, int density, int maxTimes, Color color) {
        if (!ParticleHelper.isParticleCompatibleWithDustOptions(particle)) {
            return getCircle(particle, location, radius, height, density, maxTimes);
        }

        return new TemporaryTimer(maxTimes * density) {
            double phi = 0.0;

            @Override
            public void tick() {
                phi += 2 * Math.PI / density;

                for (double theta = 0.0; theta <= 2 * Math.PI; theta += 2 * Math.PI / density) {
                    double xCord = radius * Math.cos(theta + phi);
                    double yCord = height;
                    double zCord = radius * Math.sin(theta + phi);

                    location.add(xCord, yCord, zCord);
                    location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0, new Particle.DustOptions(color, 1));
                    location.subtract(xCord, yCord, zCord);
                }
            }
        };
    }
}
