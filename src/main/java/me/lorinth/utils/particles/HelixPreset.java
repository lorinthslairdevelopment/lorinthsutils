package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;

public class HelixPreset {
    //Helix
    public static TemporaryTimer getHelix(Particle particle, Location location, double radius) {
        return getHelix(particle, location, radius, 6, 16);
    }

    public static TemporaryTimer getHelix(Particle particle, Location location, double radius, double maxHeight) {
        return getHelix(particle, location, radius, maxHeight, 16);
    }

    public static TemporaryTimer getHelix(Particle particle, Location location, double radius, double maxHeight, int density) {
        return new TemporaryTimer((int)Math.ceil(density * maxHeight / (2 * Math.PI))) {
            double time = 0;

            @Override
            public void tick() {
                time += 2 * Math.PI / density;

                double xCord = radius * Math.cos(time);
                double yCord = time;
                double zCord = radius * Math.sin(time);

                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0);
                location.subtract(xCord, yCord, zCord);
            }
        };
    }

    public static TemporaryTimer getHelix(Particle particle, Location location, double radius, double maxHeight, int density, Color color) {
        if (!ParticleHelper.isParticleCompatibleWithDustOptions(particle)) {
            return getHelix(particle, location, radius, maxHeight, density);
        }

        return new TemporaryTimer((int)Math.ceil(density * maxHeight / (2 * Math.PI))) {
            double time = 0;

            @Override
            public void tick() {
                time += 2 * Math.PI / density;

                double xCord = radius * Math.cos(time);
                double yCord = time;
                double zCord = radius * Math.sin(time);

                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0, new Particle.DustOptions(color, 1));
                location.subtract(xCord, yCord, zCord);
            }
        };
    }
}
