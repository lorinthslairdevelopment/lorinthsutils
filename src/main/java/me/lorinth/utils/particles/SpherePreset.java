package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;

public class SpherePreset {
    //Sphere
    public static TemporaryTimer getSphere(Particle particle, Location location, double radius) {
        return getSphere(particle, location, radius, 8,16, 1);
    }

    public static TemporaryTimer getSphere(Particle particle, Location location, double radius, int circlesCount) {
        return getSphere(particle, location, radius, circlesCount, 16, 1);
    }

    public static TemporaryTimer getSphere(Particle particle, Location location, double radius, int circlesCount, int particlesPerCircle) {
        return getSphere(particle, location, radius, circlesCount, particlesPerCircle, 1);
    }

    public static TemporaryTimer getSphere(Particle particle, Location location, double radius, int circlesCount, int particlesPerCircle, int maxTimes) {
        return new TemporaryTimer(maxTimes * circlesCount) {
            double phi = 0;

            @Override
            public void tick() {
                phi += Math.PI / circlesCount; //colatitude

                for (double theta = 0; theta <= 2 * Math.PI; theta += 2 * Math.PI / particlesPerCircle) { //longitude
                    double xCord = radius * Math.cos(theta) * Math.sin(phi);
                    double yCord = radius * Math.cos(phi);
                    double zCord = radius * Math.sin(theta) * Math.sin(phi);

                    location.add(xCord, yCord, zCord);
                    location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0);
                    location.subtract(xCord, yCord, zCord);
                }

                if (phi >= Math.PI) {
                    phi = 0;
                }
            }
        };
    }

    public static TemporaryTimer getSphere(Particle particle, Location location, double radius, int circlesCount, int particlesPerCircle, int maxTimes, Color color) {
        if (!ParticleHelper.isParticleCompatibleWithDustOptions(particle)) {
            return getSphere(particle, location, radius, circlesCount, particlesPerCircle, maxTimes);
        }

        return new TemporaryTimer(maxTimes * circlesCount) {
            double phi = 0;

            @Override
            public void tick() {
                phi += Math.PI / circlesCount; //colatitude

                for (double theta = 0; theta <= 2 * Math.PI; theta += 2 * Math.PI / particlesPerCircle) { //longitude
                    double xCord = radius * Math.cos(theta) * Math.sin(phi);
                    double yCord = radius * Math.cos(phi);
                    double zCord = radius * Math.sin(theta) * Math.sin(phi);

                    location.add(xCord, yCord, zCord);
                    location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0, new Particle.DustOptions(color, 1));
                    location.subtract(xCord, yCord, zCord);
                }

                if (phi >= Math.PI) {
                    phi = 0;
                }
            }
        };
    }
}
