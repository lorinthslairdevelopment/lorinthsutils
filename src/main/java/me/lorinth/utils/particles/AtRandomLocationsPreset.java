package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;

public class AtRandomLocationsPreset {
    //spawn particle at random location
    public static TemporaryTimer getParticleAtRandomLocation(Particle particle, Location location, int spawnedParticles, double maxDistance) {
        return new TemporaryTimer(spawnedParticles) {
            @Override
            public void tick() {
                double xCord = Math.random() * maxDistance * 2 - maxDistance;
                double yCord = Math.random() * maxDistance * 2 - maxDistance;
                double zCord = Math.random() * maxDistance * 2 - maxDistance;

                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0);
                location.subtract(xCord, yCord, zCord);
            }
        };
    }

    public static TemporaryTimer getParticleAtRandomLocation(Particle particle, Location location, int spawnedParticles, double maxDistance, Color color) {
        if (!ParticleHelper.isParticleCompatibleWithDustOptions(particle)) {
            return getParticleAtRandomLocation(particle, location, spawnedParticles, maxDistance);
        }

        return new TemporaryTimer(spawnedParticles) {
            @Override
            public void tick() {
                double xCord = Math.random() * maxDistance * 2 - maxDistance;
                double yCord = Math.random() * maxDistance * 2 - maxDistance;
                double zCord = Math.random() * maxDistance * 2 - maxDistance;

                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0, new Particle.DustOptions(color, 1));
                location.subtract(xCord, yCord, zCord);
            }
        };
    }
}
