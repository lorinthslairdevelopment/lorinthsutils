package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Location;
import org.bukkit.Particle;

import static me.lorinth.utils.particles.ParticleHelper.spawnParticlesInLine;

public class EightSidedStarPreset {

    public static TemporaryTimer getFixedEightSidedStar(Particle particle, Location location, int maxSpawnedParticles, double radius) {

        double root = radius*0.707;
        double sourceXCord = location.getX();
        double sourceZCord = location.getZ();
        int spawnedParticlesPerSide = maxSpawnedParticles / 8;

        return new TemporaryTimer(spawnedParticlesPerSide) {
            @Override
            public void tick() {
                handleEquation(1, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(2, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(3, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(4, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(5, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(6, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(7, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(8, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
            }
        };
    }

    private static void handleEquation(int equation, Particle particle, Location location, double sourceXCord,
                                       double sourceZCord, int spawnedParticlesPerSide, double root, double radius) {
        double step;
        double minXCord;
        double maxXCord;

        //a * XCord + b = ZCord
        double a;
        double b;

        switch (equation) {
            case 1:
                minXCord = sourceXCord - root;
                maxXCord = sourceXCord;

                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = -2.414;
                b = sourceZCord-radius-a*sourceXCord;
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 2:
                minXCord = sourceXCord - root;
                maxXCord = sourceXCord + radius;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = -0.414;
                b = sourceZCord-a*(sourceXCord+radius);
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 3:
                minXCord = sourceXCord - root;
                maxXCord = sourceXCord;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = 2.414;
                b = sourceZCord+radius-a*sourceXCord;
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 4:
                minXCord = sourceXCord;
                maxXCord = sourceXCord + root;

                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = -2.414;
                b = sourceZCord+radius-a*sourceXCord;
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 5:
                minXCord = sourceXCord - radius;
                maxXCord = sourceXCord + root;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = 0.414;
                b = sourceZCord-a*(sourceXCord-radius);
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 6:
                minXCord = sourceXCord;
                maxXCord = sourceXCord + root;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = 2.414;
                b = sourceZCord-radius-a*sourceXCord;
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 7:
                minXCord = sourceXCord - root;
                maxXCord = sourceXCord + radius;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = 0.414;
                b = sourceZCord-a*(sourceXCord+radius);
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 8:
                minXCord = sourceXCord - radius;
                maxXCord = sourceXCord + root;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = -0.414;
                b = sourceZCord-a*(sourceXCord-radius);
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
        }
    }
}
