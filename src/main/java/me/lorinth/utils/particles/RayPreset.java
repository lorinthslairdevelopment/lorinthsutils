package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class RayPreset {
    //1 Ray - helpfull for accuracy based skills
    public static TemporaryTimer getRay(Particle particle, Location location, int distance, double step) {
        return getRay(particle, location, distance, step, 1); //1.0 as default
    }

    public static TemporaryTimer getRay(Particle particle, Location location, int distance, double step, int maxTimes) {
        return new TemporaryTimer(maxTimes * distance) {
            double particleDistance = 0.0;
            Vector direction = location.getDirection().normalize();

            @Override
            public void tick() {
                if (particleDistance > distance) {
                    particleDistance = 0.0;
                }

                particleDistance += step;

                double xCord = direction.getX() * particleDistance;
                double yCord = direction.getY() * particleDistance;
                double zCord = direction.getZ() * particleDistance;

                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0);
                location.subtract(xCord, yCord, zCord);
            }
        };
    }

    public static TemporaryTimer getRay(Particle particle, Location location, int distance, double step, int maxTimes, Color color) {
        if (!ParticleHelper.isParticleCompatibleWithDustOptions(particle)) {
            return getRay(particle, location, distance, step, maxTimes);
        }

        return new TemporaryTimer(maxTimes * distance) {
            double particleDistance = 0.0;
            Vector direction = location.getDirection().normalize();

            @Override
            public void tick() {
                if (particleDistance > distance) {
                    particleDistance = 0.0;
                }

                particleDistance += step;

                double xCord = direction.getX() * particleDistance;
                double yCord = direction.getY() * particleDistance;
                double zCord = direction.getZ() * particleDistance;

                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0, new Particle.DustOptions(color, 1));
                location.subtract(xCord, yCord, zCord);
            }
        };
    }
}
