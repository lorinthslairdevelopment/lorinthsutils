package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;

public class RaindropPreset {
    //1 RainDrop
    public static TemporaryTimer getRainDrop(Particle particle, Location location, int spawnedParticles, double maxDistance, double step) {
        return new TemporaryTimer(maxDistance, step) {
            double height = maxDistance;

            @Override
            public void tick() {
                double xCord = Math.random() * maxDistance * 2 - maxDistance;
                double yCord = location.getY() + height;
                double zCord = Math.random() * maxDistance * 2 - maxDistance;

                height -= step;
                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0);
                location.subtract(xCord, yCord, zCord);
            }
        };
    }

    public static TemporaryTimer getRainDrop(Particle particle, Location location, int spawnedParticles, double maxDistance, double step, Color color) {
        if (!ParticleHelper.isParticleCompatibleWithDustOptions(particle)) {
            return getRainDrop(particle, location, spawnedParticles, maxDistance, step);
        }

        return new TemporaryTimer(maxDistance, step) {
            double height = maxDistance;

            @Override
            public void tick() {
                double xCord = Math.random() * maxDistance * 2 - maxDistance;
                double yCord = location.getY() + height;
                double zCord = Math.random() * maxDistance * 2 - maxDistance;

                height -= step;
                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0, new Particle.DustOptions(color, 1));
                location.subtract(xCord, yCord, zCord);
            }
        };
    }
}
