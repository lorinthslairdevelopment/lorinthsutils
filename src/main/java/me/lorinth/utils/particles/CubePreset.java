package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;

public class CubePreset {
    public static TemporaryTimer getFixedCube(Location location, Particle particle, double radius, int maxParticles) {
        double root = radius / 2;
        int spawnedParticlesPerSide = maxParticles / 12;

        return new TemporaryTimer(spawnedParticlesPerSide) {
            @Override
            public void tick() {
                /*

                1
        _________________
        \               /
         \             /
          \           /
        2  \         / 3
            \       /                           5/             \6
             \     /                            /               \
              \   /                             _________________
               \ /                                    4

                */
                handleEquation(1, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(2, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(3, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(4, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(5, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(6, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(7, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(8, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(9, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(10, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(11, particle, location, spawnedParticlesPerSide, root, radius);
                handleEquation(12, particle, location, spawnedParticlesPerSide, root, radius);
            }
        };
    }

    private static void handleEquation(int equation, Particle particle, Location location, int spawnedParticlesPerSide, double root, double radius) {
        World world = location.getWorld();
        double sourceXCord = location.getX();
        double sourceYCord = location.getY();
        double sourceZCord = location.getZ();
        double step = radius / spawnedParticlesPerSide;

        switch (equation) {
            case 1:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newXCord = sourceXCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, newXCord, sourceYCord + root, sourceZCord + root),
                            0, 0, 0, 0, 0);
                }
                break;
            case 2:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newYCord = sourceYCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, sourceXCord + root, newYCord, sourceZCord + root),
                            0, 0, 0, 0, 0);
                }
                break;
            case 3:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newXCord = sourceXCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, newXCord, sourceYCord - root, sourceZCord + root),
                            0, 0, 0, 0, 0);
                }
                break;
            case 4:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newYCord = sourceYCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, sourceXCord - root, newYCord, sourceZCord + root),
                            0, 0, 0, 0, 0);
                }
                break;
            case 5:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newXCord = sourceXCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, newXCord, sourceYCord + root, sourceZCord - root),
                            0, 0, 0, 0, 0);
                }
                break;
            case 6:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newYCord = sourceYCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, sourceXCord + root, newYCord, sourceZCord - root),
                            0, 0, 0, 0, 0);
                }
                break;
            case 7:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newXCord = sourceXCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, newXCord, sourceYCord - root, sourceZCord - root),
                            0, 0, 0, 0, 0);
                }
                break;
            case 8:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newYCord = sourceYCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, sourceXCord - root, newYCord, sourceZCord - root),
                            0, 0, 0, 0, 0);
                }
                break;
            case 9:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newZCord = sourceZCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, sourceXCord - root, sourceYCord + root, newZCord),
                            0, 0, 0, 0, 0);
                }
                break;
            case 10:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newZCord = sourceZCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, sourceXCord + root, sourceYCord + root, newZCord),
                            0, 0, 0, 0, 0);
                }
                break;
            case 11:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newZCord = sourceZCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, sourceXCord + root, sourceYCord - root, newZCord),
                            0, 0, 0, 0, 0);
                }
                break;
            case 12:
                for (int i = 0; i <= spawnedParticlesPerSide; i++) {
                    double newZCord = sourceZCord - root + i * step;

                    world.spawnParticle(particle, new Location(world, sourceXCord - root, sourceYCord - root, newZCord),
                            0, 0, 0, 0, 0);
                }
                break;
        }
    }
}
