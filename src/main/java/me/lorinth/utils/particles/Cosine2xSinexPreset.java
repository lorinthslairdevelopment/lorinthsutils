package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;

public class Cosine2xSinexPreset {
    //cos(2x)sin(x) //best on eye location
    public static TemporaryTimer getConine2xSinex(Particle particle, Location location, double radius) {
        return getConine2xSinex(particle, location, radius, 16, 1);
    }

    public static TemporaryTimer getConine2xSinex(Particle particle, Location location, double radius, int density) {
        return getConine2xSinex(particle, location, radius, density, 1);
    }

    public static TemporaryTimer getConine2xSinex(Particle particle, Location location, double radius, int density, int maxTimes) {
        return new TemporaryTimer(maxTimes * density) {
            double time = 0;

            @Override
            public void tick() {
                time += 2 * Math.PI / density;

                double xCord = radius * Math.cos(time);
                double yCord = Math.cos(2 * time) * Math.sin(time);
                double zCord = radius * Math.sin(time);

                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0);
                location.subtract(xCord, yCord, zCord);
            }
        };
    }

    public static TemporaryTimer getConine2xSinex(Particle particle, Location location, double radius, int density, int maxTimes, Color color) {
        if (!ParticleHelper.isParticleCompatibleWithDustOptions(particle)) {
            return getConine2xSinex(particle, location, radius, density, maxTimes);
        }

        return new TemporaryTimer(maxTimes * density) {
            double time = 0;

            @Override
            public void tick() {
                time += 2 * Math.PI / density;

                double xCord = radius * Math.cos(time);
                double yCord = Math.cos(2 * time) * Math.sin(time);
                double zCord = radius * Math.sin(time);

                location.add(xCord, yCord, zCord);
                location.getWorld().spawnParticle(particle, location, 0, 0, 0, 0, 0, new Particle.DustOptions(color, 1));
                location.subtract(xCord, yCord, zCord);
            }
        };
    }
}
