package me.lorinth.utils.particles;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;

public class ParticleHelper {

    public static Particle toParticle(String value) {
        switch (preParseParticleName(value)) {
            case "BARRIER":
                return Particle.BARRIER;
            case "BLOCK_CRACK":
                return Particle.BLOCK_CRACK;
            case "BLOCK_DUST":
                return Particle.BLOCK_DUST;
            case "BUBBLE_COLUMN_UP":
                return Particle.BUBBLE_COLUMN_UP;
            case "BUBBLE_POP":
                return Particle.BUBBLE_POP;
            case "CAMPFIRE_COSY_SMOKE":
                return Particle.CAMPFIRE_COSY_SMOKE;
            case "CAMPFIRE_SIGNAL_SMOKE":
                return Particle.CAMPFIRE_SIGNAL_SMOKE;
            case "CLOUD":
                return Particle.CLOUD;
            case "COMPOSTER":
                return Particle.COMPOSTER;
            case "CRIT":
                return Particle.CRIT;
            case "CRIT_MAGIC":
                return Particle.CRIT_MAGIC;
            case "CURRENT_DOWN":
                return Particle.CURRENT_DOWN;
            case "DAMAGE_INDICATOR":
                return Particle.DAMAGE_INDICATOR;
            case "DOLPHIN":
                return Particle.DOLPHIN;
            case "DRAGON_BREATH":
                return Particle.DRAGON_BREATH;
            case "DRIP_LAVA":
                return Particle.DRIP_LAVA;
            case "DRIP_WATER":
                return Particle.DRIP_WATER;
            case "DRIPPING_HONEY":
                return Particle.DRIPPING_HONEY;
            case "ENCHANTMENT_TABLE":
                return Particle.ENCHANTMENT_TABLE;
            case "END_ROD":
                return Particle.END_ROD;
            case "EXPLOSION_HUGE":
                return Particle.EXPLOSION_HUGE;
            case "EXPLOSION_LARGE":
                return Particle.EXPLOSION_LARGE;
            case "EXPLOSION_NORMAL":
                return Particle.EXPLOSION_NORMAL;
            case "FALLING_DUST":
                return Particle.FALLING_DUST;
            case "FALLING_HONEY":
                return Particle.FALLING_HONEY;
            case "FALLING_LAVA":
                return Particle.FALLING_LAVA;
            case "FALLING_NECTAR":
                return Particle.FALLING_NECTAR;
            case "FALLING_WATER":
                return Particle.FALLING_WATER;
            case "FIREWORKS_SPARK":
                return Particle.FIREWORKS_SPARK;
            case "FLAME":
                return Particle.FLAME;
            case "FLASH":
                return Particle.FLASH;
            case "HEART":
                return Particle.HEART;
            case "ITEM_CRACK":
                return Particle.ITEM_CRACK;
            case "LANDING_HONEY":
                return Particle.LANDING_HONEY;
            case "LANDING_LAVA":
                return Particle.LANDING_LAVA;
            case "LAVA":
                return Particle.LAVA;
            case "LEGACY_BLOCK_CRACK":
                return Particle.LEGACY_BLOCK_CRACK;
            case "LEGACY_BLOCK_DUST":
                return Particle.LEGACY_BLOCK_DUST;
            case "LEGACY_FALLING_DUST":
                return Particle.LEGACY_FALLING_DUST;
            case "MOB_APPEARANCE":
                return Particle.MOB_APPEARANCE;
            case "NAUTILUS":
                return Particle.NAUTILUS;
            case "NOTE":
                return Particle.NOTE;
            case "PORTAL":
                return Particle.PORTAL;
            case "REDSTONE":
                return Particle.REDSTONE;
            case "SLIME":
                return Particle.SLIME;
            case "SMOKE_LARGE":
                return Particle.SMOKE_LARGE;
            case "SMOKE_NORMAL":
                return Particle.SMOKE_NORMAL;
            case "SNEEZE":
                return Particle.SNEEZE;
            case "SNOW_SHOVEL":
                return Particle.SNOW_SHOVEL;
            case "SNOWBALL":
                return Particle.SNOWBALL;
            case "SPELL":
                return Particle.SPELL;
            case "SPELL_INSTANT":
                return Particle.SPELL_INSTANT;
            case "SPELL_MOB":
                return Particle.SPELL_MOB;
            case "SPELL_MOB_AMBIENT":
                return Particle.SPELL_MOB_AMBIENT;
            case "SPELL_WITCH":
                return Particle.SPELL_WITCH;
            case "SPIT":
                return Particle.SPIT;
            case "SQUID_INK":
                return Particle.SQUID_INK;
            case "SUSPENDED":
                return Particle.SUSPENDED;
            case "SUSPENDED_DEPTH":
                return Particle.SUSPENDED_DEPTH;
            case "SWEEP_ATTACK":
                return Particle.SWEEP_ATTACK;
            case "TOTEM":
                return Particle.TOTEM;
            case "TOWN_AURA":
                return Particle.TOWN_AURA;
            case "VILLAGER_ANGRY":
                return Particle.VILLAGER_ANGRY;
            case "VILLAGER_HAPPY":
                return Particle.VILLAGER_HAPPY;
            case "WATER_BUBBLE":
                return Particle.WATER_BUBBLE;
            case "WATER_DROP":
                return Particle.WATER_DROP;
            case "WATER_SPLASH":
                return Particle.WATER_SPLASH;
            case "WATER_WAKE":
                return Particle.WATER_WAKE;
        }

        return null;
    }

    private static String preParseParticleName(String value) {
        return value.toUpperCase().replace(' ', '_');
    }

    public static boolean isParticleCompatibleWithDustOptions(Particle particle) {
        return particle.equals(Particle.REDSTONE);
    }

    public static void spawnParticlesInLine(int spawnedParticlesPerSide, double minXCord, double step, double a, double b, Location location, Particle particle) {
        for (int i = 0; i <= spawnedParticlesPerSide; i++) {
            double newXCord = minXCord + i * step;
            double newZCord = a * newXCord + b;

            World world = location.getWorld();

            world.spawnParticle(particle, new Location(world, newXCord, location.getY(), newZCord), 0, 0, 0, 0, 0);
        }
    }
}
