package me.lorinth.utils.particles;

import me.lorinth.utils.tasks.TemporaryTimer;
import org.bukkit.Location;
import org.bukkit.Particle;

import static me.lorinth.utils.particles.ParticleHelper.spawnParticlesInLine;

public class SixSidedStarPreset {

    //6 sided star
    public static TemporaryTimer getFixedSixSidedStar(Particle particle, Location location, int maxSpawnedParticles, double radius) {

        double root = radius*0.866;
        double sourceXCord = location.getX();
        double sourceZCord = location.getZ();
        int spawnedParticlesPerSide = maxSpawnedParticles / 6;

        return new TemporaryTimer(spawnedParticlesPerSide) {
            @Override
            public void tick() {
                /*

                1
        _________________
        \               /
         \             /
          \           /
        2  \         / 3
            \       /                           5/             \6
             \     /                            /               \
              \   /                             _________________
               \ /                                    4

                */
                handleEquation(1, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(2, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(3, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(4, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(5, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
                handleEquation(6, particle, location, sourceXCord, sourceZCord, spawnedParticlesPerSide, root, radius);
            }
        };
    }

    private static void handleEquation(int equation, Particle particle, Location location, double sourceXCord,
                                       double sourceZCord, int spawnedParticlesPerSide, double root, double radius) {
        double step;
        double minXCord;
        double maxXCord;

        //a * XCord + b = ZCord
        double a;
        double b;

        switch (equation) {
            case 1:
                minXCord = sourceXCord - root;
                maxXCord = sourceXCord + root;

                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = 0;
                b = sourceZCord + radius / 2;
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 2:
                minXCord = sourceXCord - root;
                maxXCord = sourceXCord;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                //3/sqrt(3)=~1.732   (~1,732050807568877293527446341506) 4th decimal place accuracy
                a = -1.732; //- 3 / Math.sqrt(3);
                b = sourceZCord - radius - (a * sourceXCord);
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 3:
                minXCord = sourceXCord;
                maxXCord = sourceXCord + root;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = 1.732; //3 / Math.sqrt(3);
                b = sourceZCord - radius - (a * sourceXCord);
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 4:
                minXCord = sourceXCord - root;
                maxXCord = sourceXCord + root;

                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = 0;
                b = sourceZCord - radius / 2;
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 5:
                minXCord = sourceXCord - root;
                maxXCord = sourceXCord;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = 1.732; //3 / Math.sqrt(3);
                b = sourceZCord + radius - (a * sourceXCord);
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
            case 6:
                minXCord = sourceXCord;
                maxXCord = sourceXCord + root;
                step = Math.abs(maxXCord - minXCord) / spawnedParticlesPerSide;

                a = -1.732; //- 3 / Math.sqrt(3);
                b = sourceZCord + radius - (a * sourceXCord);
                spawnParticlesInLine(spawnedParticlesPerSide, minXCord, step, a, b, location, particle);
                break;
        }
    }
}
