import me.lorinth.utils.Calculator;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTests {

    @Test(expected = IllegalArgumentException.class)
    public void eval_WhenLnArgumentIsNegative_ThrowsException() {
        Calculator.eval(null, "ln(-1)");
    }

    @Test (expected = IllegalArgumentException.class)
    public void eval_WhenLnArgumentIsZero_ThrowsException() {
        Calculator.eval(null, "ln(0)");
    }

    @Test (expected = IllegalArgumentException.class)
    public void eval_WhenLogBaseIsOne_ThrowsException() {
        Calculator.eval(null, "log(20, 1.0)");
    }

    @Test
    public void eval_WhenCalled_ReturnsRandomValue() {
        Assert.assertEquals(31, Calculator.eval(null, "rand(31, 30)"), 1);
    }

    @Test
    public void eval_WhenCalled_ReturnsRandomValueFromExponentialDistributionGreaterThanZero() {
        Assert.assertTrue((Calculator.eval(null, "randexp(1/6)")) >= 0);
    }

    @Test
    public void eval_WhenCalled_ReturnsRandomValueFromGammaDistributionGreaterThanZero() {
        Assert.assertTrue((Calculator.eval(null, "randg(2, 1/6)")) >= 0);
    }
}
