import me.lorinth.utils.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CalculatorParametrizedTests
{
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                //power
                { "2^0", 1.0 }, { "2^1", 2.0 }, { "2^2", 4.0 }, { "2^4", 16.0 }, { "12^2", 144.0 }, { "(1/2)^2", 0.25 },
                { "2^(-1)", 0.5 }, { "2^(1/2)", 1.41421356 },
                //parseAbsoluteValue
                { "abs(0)", 0.0 }, { "abs(-1)", 1.0 }, { "abs(1)", 1.0 }, { "abs(-10)", 10.0 },
                { "abs(4)", 4.0 }, { "abs(5)", 5.0 }, { "abs(-1000000)", 1000000.0 },
                //parseNaturalLogarithm
                { "ln(1)", 0.0 }, { "ln(2.7182)", 1.0 }, { "ln(7.3890)", 2.0 }, { "ln(1.6487)", 0.5},
                //parseLogarithm
                { "log(1)", 0.0 }, { "log(10)", 1.0 }, { "log(100)", 2.0 }, { "log(1/10)", -1 },
                { "log(1, 2)", 0.0 }, { "log(2, 2)", 1.0 }, { "log(32, 2)", 5.0 }, { "log(1/4, 2)", -2 },
                //parseExp
                { "exp(0)", 1.0 }, { "exp(-10000)", 0.0 }, { "exp(1)", 2.7182 }, { "exp(0.5)", 1.6487 },
                //parseFloor
                { "floor(0.5)", 0.0 }, { "floor(9.999)", 9.0 }, { "floor(-9.999)", -10.0 }, { "floor(1.0001)", 1.0 },
                //parseCeil
                { "ceil(0.5)", 1.0 }, { "ceil(9.999)", 10.0 }, { "ceil(-9.999)", -9.0 }, { "ceil(1.0001)", 2.0 },
                //parseModulo
                { "mod(1, 2)", 1.0 }, { "mod(2, 2)", 0.0 }, { "mod(32, 7)", 4.0 }, { "mod(130, 3)", 1.0 },

                //other
                { "math.min(0.9, (100.0 - (math.max(0, ((0.0 + (0.0 * (0.0 / 100.0))) - 0.0)))) / 100.0)", 0.9 },
                { "math.max(0.5 * 0, 0 * ((100 - 10 * (20 - 10)) / 100.0))", 0.0},
                { "max(0.5 * 0, 0 * ((100 - 10 * (20 - 10)) / 100.0))", 0.0},
                { "-1", -1.0 }
        });
    }

    @Parameterized.Parameter // first data value (0) is default
    public /* NOT private */ String formula;

    @Parameterized.Parameter(1)
    public /* NOT private */ double result;

    @Test
    public void eval_WhenCalled_ReturnsValue() {
        Assert.assertEquals(result, Calculator.eval(null, formula), 0.0001);
    }
}
